<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\TypeDeclaration\Rector\ClassMethod\AddVoidReturnTypeWhereNoReturnRector;

return RectorConfig::configure()
    ->withPaths([
        __DIR__ . '/config',
        __DIR__ . '/public',
        __DIR__ . '/src',
        __DIR__ . '/tests',
    ])
    // uncomment to reach your current PHP version
    ->withPhpSets()
    ->withAttributesSets(symfony: true, doctrine: true)
    ->withSets([
        \Rector\Symfony\Set\SymfonySetList::SYMFONY_63,
        \Rector\Symfony\Set\SymfonySetList::SYMFONY_CODE_QUALITY,
        \Rector\Symfony\Set\SymfonySetList::SYMFONY_CONSTRUCTOR_INJECTION
    ])
    ->withSymfonyContainerPhp(__DIR__ . '/tests/symfony-container.php')
    ->registerService(
        \Rector\Symfony\Bridge\Symfony\Routing\SymfonyRoutesProvider::class,
        \Rector\Symfony\Contract\Bridge\Symfony\Routing\SymfonyRoutesProviderInterface::class
    )
    ->withRules([
        AddVoidReturnTypeWhereNoReturnRector::class,
    ]);

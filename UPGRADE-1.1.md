UPGRADE TO 1.1.x
=======================

The guide [docs/upgrade.md](docs/upgrade.md) explains the main steps to upgrade the app.
This upgrade note explains the specific steps to upgrade from the version 1.0 to the version 1.1.x.

## New environment variables

```bash
MAILER_DSN=null://null
MAILER_SENDER=admin@loot.org
```

### About the database upgrade

The database upgrade system has changed since the version 1.0 and the namespace of migration too.
If you have installed the version 0.x, yYou may have two tables to manage migrations :

- `doctrine_migration_versions` : the table of the migrations of the app
- `migration_versions` : the table of the migrations of the library Doctrine

So before to upgrade the app :

1. remove the table `migration_versions` and `doctrine_migration_versions` if it exists
2. add the existing migrations to the `doctrine_migration_versions` without playing them

```bash
php bin/console doctrine:migrations:version --add --all
```

And after, you can upgrade the app code and run the migrations :

```bash
php loot/bin/console doctrine:migrations:migrate
```
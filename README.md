# Loot : the application to manage contributive budget

Loot is a mobile web application to track and share contributions and their rewards within a community.

To test the application, [you can use the demo](http://loot-demo.soletic.org/).

## Install

To install locally the application, you can browse the following tutorials.
Get inspiration to adapt them to your particular case.

- [Basic instructions to install Loot](docs/install.md)
- [Docker Compose support to setup a developpment environment](support/docker/dev/Readme.md)
- [Run loot on Microsoft Windows with WSL](support/ms-windows/wsl/Readme.md)

**Extend Loot for your personnal use case**

Loot is an application in a very work in progress state so features to extend are limited. It's a Symfony Application
so you can use features of Symfony to extend and configure a fork of the application.

We currently offer a simple mechanism for installing plugins as a bundle.
[Read specific documentation to learn about it](docs/plugins.md)

## Upgrade

- [Upgrare an existing loot instance](docs/upgrade.md)

## User documentation

- [Use loot - French](https://loot-project.gitlab.io/userguide)
- Contribute to the documentation : [userguide repository](https://gitlab.com/loot-project/userguide)

## API

An read-only API is under development using API Platform. The purpose is to give tiers-developers the ability to create dashboard and extract data for analysis without heavy internal coding in the core app.
A demo dashboard is under development [here](https://gitlab.com/loot-project/loot-dashboard) 

## Contributing

Public Trello board : https://trello.com/b/vE0RhXAE/loot

We have published [rules to have a community driven working on features](https://trello.com/c/WOC8UQZR/110-gouvernance-des-%C3%A9volutions-de-loot-pour-les-communaut%C3%A9s-utilisatrices).

### For developpers

- [Developpers documentation](docs/developpers.md)

Fork the repository and open merge request.

To install locally the application and test your work, you can follow these tutorials :

- [Docker Compose support to setup a developpment environment](support/docker/dev/Readme.md)
- [Run loot on Microsoft Windows with WSL](support/ms-windows/wsl/Readme.md)

## Maintainers et supporters

- [Laurent Chedanne](https://blog.chedanne.pro) - Maintener
- Pierre Trendel - Maintener
- Florent Kaisser - Maintener
- [ANIS](http://anis.asso.fr/) - Financial supporter for the first release

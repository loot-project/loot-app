#!/bin/bash

# phpstan
docker run --rm -v $(pwd):/app -w /app -e CI=true jakzal/phpqa:php8.2 phpstan analyse ./src

# twig-lint
docker run --rm -v $(pwd):/app -w /app -e CI=true jakzal/phpqa:php8.2 twig-lint lint ./templates

# php coding standards
# docker run --rm -v $(pwd):/app -w /app -e CI=true jakzal/phpqa:php8.2 phpcs -v --standard=PSR12 --ignore=./src/Kernel.php ./src
<?php

declare(strict_types=1);

namespace    migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211216134218 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE activity (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE activity_budget (activity_id INT NOT NULL, budget_id INT NOT NULL, INDEX IDX_2184177B81C06096 (activity_id), INDEX IDX_2184177B36ABA6B8 (budget_id), PRIMARY KEY(activity_id, budget_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE activity_contribution (activity_id INT NOT NULL, contribution_id INT NOT NULL, INDEX IDX_E38DDC5481C06096 (activity_id), INDEX IDX_E38DDC54FE5E5FBD (contribution_id), PRIMARY KEY(activity_id, contribution_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE activity_budget ADD CONSTRAINT FK_2184177B81C06096 FOREIGN KEY (activity_id) REFERENCES activity (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE activity_budget ADD CONSTRAINT FK_2184177B36ABA6B8 FOREIGN KEY (budget_id) REFERENCES budget (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE activity_contribution ADD CONSTRAINT FK_E38DDC5481C06096 FOREIGN KEY (activity_id) REFERENCES activity (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE activity_contribution ADD CONSTRAINT FK_E38DDC54FE5E5FBD FOREIGN KEY (contribution_id) REFERENCES contribution (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE `activity` ADD UNIQUE(`title`);');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE activity_budget DROP FOREIGN KEY FK_2184177B81C06096');
        $this->addSql('ALTER TABLE activity_contribution DROP FOREIGN KEY FK_E38DDC5481C06096');
        $this->addSql('DROP TABLE activity');
        $this->addSql('DROP TABLE activity_budget');
        $this->addSql('DROP TABLE activity_contribution');
    }
}

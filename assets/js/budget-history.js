List = require('list.js');

$(document).ready(function(e){

    var options = {
        valueNames: [ 'log-amount','log-full-date' ]
    };

    var loglist = new List('log-list-container', options);

    $('#sort-by-amount').on('click',function(e){
        $('.sort-button').removeClass('sortactive');
        var sortorder = $(this).data('sortorder')=='asc'?'desc':'asc';
        loglist.sort('log-amount', { order: sortorder }); 
        $(this).data('sortorder',sortorder);
        $(this).addClass('sortactive');
        $(this).find('i').removeClass();
        if(sortorder=='asc') {
            $(this).find('i').addClass('icon-arrow-up');
        }else if(sortorder=='desc') {
            $(this).find('i').addClass('icon-arrow-down');
        }
    })

    // Show / Hide only my contribution
    $('#log-show-mine').on('change',function(){
        var is_checked = $(this).is(':checked');
        if(is_checked) {
            $('.log-list-item').each(function(index,elt){
                if(!$(elt).hasClass('me')) {
                    $(this).hide();
                }
            })    
        }else{
            $('.log-list-item').show();
        }
        
        
    })

    // Show / Hide claimed contribution
    $('#log-hide-claimed').on('change',function(){
        var is_checked = $(this).is(':checked');
        if(is_checked) {
            $('.log-list-item').each(function(index,elt){
                if($(elt).hasClass('log-claimed')) {
                    $(this).hide();
                }
            })    
        }else{
            $('.log-list-item').show();
        }
        
        
    })

});

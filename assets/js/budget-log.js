require('./budget-edit');

$(document).ready(function(e){
            
    $('input[name="form[rewarded]"]').on('change',function(){
        $('.form-group.log-amount').toggle();
    });

    if ($('input[name="form[rewarded]"]:checked').val() == "0")
        $('.form-group.log-amount').toggle();

});

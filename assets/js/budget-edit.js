require('select2');

jQuery(function($) {
    $('.select-activities').each(function() {
        const $el = $(this);
        const allowAdd = !! $el.data('allow-add');

        const options = {
            language: $('html').attr('lang'),
            dropdownParent: $('.main-content'),
            tags: allowAdd,
            tokenSeparators: [',', ' '],
            createTag: function(params) {
                const term = $.trim(params.term);
                if (term === '') {
                    return null;
                }
                // New items start with `_` and will be handled 
                // by the corresponding `DataTransformer` PHP class.
                return {
                    id: '_' + term,
                    text: term
                };
            }
        };

        $el.select2(options);
    });
});

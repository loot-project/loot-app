require("bootstrap-select");
List = require('list.js');

$(document).ready(function(e){

    if ($('#budget-list .list > div').length > 0) {
        let budgets = new List('budget-list', {
            valueNames: [
                {data: ['tags', 'archived', 'contributor']},
            ]
        });

        function filterBudget() {
            let isUserOnly = $('#budget-owned-checkbox').is(':checked');
            let isArchived = $('#budget-archived-checkbox').is(':checked');
            let searchedTags = $('#budget-tag-search').val();
            budgets.filter(function(item) {
                let tagsValid = true;
                if(searchedTags.length>0) {
                    $.each(searchedTags, function(searchTagIndex, searchTagValue) {
                        tagsValid = tagsValid && (item.values().tags.indexOf(searchTagValue) >= 0);
                    });
                }
                let show_budget = ((isUserOnly && item.values().contributor == '1') || !isUserOnly)
                    && ((isArchived && item.values().archived == '1') || (!isArchived && item.values().archived == '0'))
                    && tagsValid;
                if (show_budget) {
                    $(item.elm).collapse('show');
                } else {
                    $(item.elm).collapse('hide');
                }
                return (show_budget);
            });
            filterMessageResult();
        }

        function filterMessageResult() {
            let isUserOnly = $('#budget-owned-checkbox').is(':checked');
            $('#budget-list > p').remove();
            if (budgets.visibleItems.length <= 0) {
                if (isUserOnly) {
                    $('#budget-list').append('<p class="mt-1 alert alert-info">' + Translator.trans("budget.list.nobudget_forme", {}, "loot") + '</p>');
                    $('#budget-list > p').append('<br /><a href="#" onclick="javascript: document.getElementById(\'budget-owned-checkbox\').click(); return false;">' + Translator.trans("budget.list.view_opened_budgets", {}, "loot") + '</a>');
                } else {
                    $('#budget-list').append('<p class="mt-1 alert alert-info">' + Translator.trans("budget.list.nobudget", {}, "loot") + '</p>');
                }
            }
        }
        filterBudget();
        $('#budget-owned-checkbox,#budget-archived-checkbox').on('change',function(e){
            filterBudget();
        });
        $('#budget-tag-search').on('changed.bs.select',function(e){
            filterBudget()
        });
    }

});
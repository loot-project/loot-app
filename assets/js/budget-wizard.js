$(document).ready(function(e){

    let totalStep = $(".budget-wizard .form-wizard-section").length + 1;
    let currentStep = $('.budget-wizard').data("fw-step");

    $(".budget-wizard .form-wizard-submission").hide();
    $(".budget-wizard .fws-legend-submission").hide();
    
    $(".budget-wizard").prepend("<div class='form-tunnel'><ul>");
    for (let i=1; i<totalStep; i++) {
        $(".budget-wizard .form-tunnel ul").append("<li class='step'>"+i+"</li>")
    }
    $(".budget-wizard .form-tunnel ul").append("<li class='step'><i class='icon-paper-plane'></i></li>")
    
    $(".budget-wizard").prepend("</ul></div>");

    displayFormStepSection(1);
    
    function skipWizard(){
        $('.budget-wizard').data("fw-step",totalStep);
        $(".budget-wizard .form-wizard-section").addClass("fws-is-finished");
        $(".budget-wizard .form-wizard-submission").show();
        displayFormStepSection(totalStep);
    }
    function displayFormStepSection(step){
        let domStep = step - 1;
        $(".budget-wizard .form-wizard-section").removeClass("fws-is-active");
        $(".budget-wizard .form-wizard-section:eq("+domStep+")").addClass("fws-is-active");
        $(".budget-wizard .form-tunnel ul li").removeClass("current-step");
        $(".budget-wizard .form-tunnel ul li:eq("+domStep+")").addClass("current-step");

        if(domStep == 0 ){
            $(".budget-wizard .form-wizard-navigation .btn-prev").prop('disabled',true);
            $(".budget-wizard .form-wizard-navigation .btn-next").prop('disabled',false);
            $(".budget-wizard .skip-wizard").show();
            $(".budget-wizard .fws-legend-submission").hide();
        }else if(domStep == totalStep-1 ){
            $(".budget-wizard .form-wizard-navigation .btn-prev").prop('disabled',false);
            $(".budget-wizard .form-wizard-navigation .btn-next").prop('disabled',true);
            $(".budget-wizard .skip-wizard").hide();
            $(".budget-wizard .fws-legend-submission").show();
        }else{
            $(".budget-wizard .form-wizard-navigation .btn-prev").prop('disabled',false);
            $(".budget-wizard .form-wizard-navigation .btn-next").prop('disabled',false);
            $(".budget-wizard .skip-wizard").hide();
            $(".budget-wizard .fws-legend-submission").hide();
        }
    }

    $('.budget-wizard .skip-wizard').on('click',function(e){
        e.preventDefault();
        skipWizard();
    })

    $('.budget-wizard .btn-next').on('click',function(e){
        currentStep = $('.budget-wizard').data("fw-step");
        $(".budget-wizard .form-wizard-section").removeClass("fws-is-finished");
        $(".budget-wizard .form-wizard-submission").hide();
        
        let domStep = currentStep - 1;
        let inputs = $(".budget-wizard .form-wizard-section:eq("+domStep+") input");
        let isStepValid = true;
        let validation_msg ="";
        inputs.each(function(index){
            if(!$(this).get(0).checkValidity()){
                console.log($(this).get(0).validationMessage);
                $(this).parent().append('<div class="invalid-feedback" style="display:block">'+$(this).get(0).validationMessage+'</div>');
                
            }
            isStepValid = $(this).get(0).checkValidity() && isStepValid;
        });
        
        if(isStepValid) {
            $(".budget-wizard .invalid-feedback").remove();
            if(currentStep<totalStep) {
                currentStep ++;
                $('.budget-wizard').data("fw-step",currentStep);
                displayFormStepSection(currentStep);
            }
            if(currentStep==totalStep) {
                $(".budget-wizard .form-wizard-section").addClass("fws-is-finished");
                $(".budget-wizard .form-wizard-submission").show();
            }
        }

        
          
    })

    $('.budget-wizard .btn-prev').on('click',function(e){
        currentStep = $('.budget-wizard').data("fw-step");
        $(".budget-wizard .form-wizard-section").removeClass("fws-is-finished");
        $(".budget-wizard .form-wizard-submission").hide();

        if(currentStep>0) {
            currentStep --;
            $('.budget-wizard').data("fw-step",currentStep);
            displayFormStepSection(currentStep);
        } 
    });

    // No wizard if edit or on posted form
    if ($('.budget.budget-edit').length || $('.budget.budget-POST').length) {
        skipWizard();
    }
});
$(document).ready(function(e){
            
    function updateClaimForm(){
        var total = 0;
        var ids = [];
        
        $('.log-select input[type="checkbox"]:checked').each(function(index,elt){
            total += $(elt).data('amount');
            ids.push($(elt).data('id'));
        });
        if ($('#claim-amount').length > 0)
            $('#claim-amount').html(total);
        $('#claim-bill #claim-ids').val(ids.join()); 
    }

    $('.log-select input[type="checkbox"]').on('change',function(){
        updateClaimForm();    
    })

    $('#log-select-all').on('change',function(){
        var is_checked = $(this).is(':checked');

        $('.log-select input[type="checkbox"]').each(function(index,elt){
           $(elt).prop('checked',is_checked);
        })

        updateClaimForm();    
    })


    updateClaimForm();
});

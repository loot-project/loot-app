<?php
/**
 * Author: Florent Kaisser.
 */

namespace App\Tests\Functionnals;

use App\Entity\User;
use App\Test\Entity\UserTestTrait;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;

class ResetPasswordControllerTest extends WebTestCase
{
    use UserTestTrait;

    /**
     * Test the reset password process with the loot-specific customization.
     *
     * - Loot translation for validators and reset form fields
     * - Flash messages on invalid request
     * - Mail sending with the enveloppe of the mailer     *
     */
    public function testRequestAndReset(): void
    {
        $client = static::createClient();

        /*
         * Request for an invalid user
         */
        $crawler = $client->request(Request::METHOD_GET, '/reset-password');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filterXPath('.//title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        $form = $crawler->filterXPath('.//form')->first()->form();
        $form['reset_password_request_form[email]'] = 'alonzo@domain.com';
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filterXPath('.//title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/reset-password/check-email'));
        $this->assertEmailCount(0);

        /*
         * Request for a valid user
         */
        $em = $client->getContainer()->get('doctrine')->getManager();
        $alonzo = $this->createUser('alonzo', $em);
        $alonzo->setEmail('alonzo@domain.com');
        $em->persist($alonzo);
        $em->flush();

        $crawler = $client->request(Request::METHOD_GET, '/reset-password');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filterXPath('.//title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        // Get refreshed session for next request and test
        $session = $client->getRequest()->getSession();
        $form = $crawler->filterXPath('.//form')->first()->form();
        $form['reset_password_request_form[email]'] = 'alonzo@domain.com';
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filterXPath('.//title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/reset-password/check-email'));
        $this->assertEmailCount(1);
        /** @var TemplatedEmail $sentMail */
        $sentMail = $this->getMailerMessage(0);
        $this->assertEmailHeaderSame($sentMail, 'To', 'alonzo@domain.com');

        // Mail sending with the enveloppe of the mailer (and not with a from header)
        $this->assertCount(0, $sentMail->getFrom(), "The mail should not have a 'from' header");

        // Test mail having the reset link
        $urls = [];
        preg_match_all("/\b(?:(?:https?):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $sentMail->getHtmlBody(), $urls);
        $this->assertEquals(1, count($urls));
        $this->assertEquals(2, count($urls[0]));
        $reset_url = $urls[0][0];
        $this->assertStringContainsString('/reset-password/reset/', $reset_url);

        /*
         * Request again too fast (test the throttling)
         */

        $crawler = $client->request(Request::METHOD_GET, '/reset-password');
        $form = $crawler->filterXPath('.//form')->first()->form();
        $form['reset_password_request_form[email]'] = 'alonzo@domain.com';
        $crawler = $client->submit($form);
        $this->assertTrue($client->getResponse()->isRedirect('/reset-password'), 'The throttling should redirect to the same page');
        $this->assertCount(1, $session->getFlashBag()->get('danger'), 'The throttling should add a flash message');

        /*
         * Follow redirect to test the flash message displays
         */
        $translator = $client->getContainer()->get('translator');
        $crawler = $client->followRedirect();
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filterXPath('.//div[contains(@class, "alert-danger")]'), 'The throttling should add a flash message');
        // Message should be a specific translation
        $this->assertStringContainsString(
            $translator->trans(ResetPasswordExceptionInterface::MESSAGE_PROBLEM_HANDLE, [], 'ResetPasswordBundle'),
            $crawler->filterXPath('.//div[contains(@class, "alert-danger")]')->text(),
            'Flash message about the throttling expected'
        );

        /*
         * Test the reset page
         */
        $crawler = $client->request(Request::METHOD_GET, $reset_url);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filterXPath('.//title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/reset-password/reset'));
        $crawler = $client->followRedirect();
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        $form = $crawler->filterXPath('.//form')->first()->form();

        // Test validator translation
        $crawler = $client->submit($form);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filterXPath('.//span[contains(@class, "form-error-message")]'), 'The form should display an error message');
        $this->assertEquals($translator->trans('password.mail_needed', [], 'validators'), $crawler->filterXPath('.//span[contains(@class, "form-error-message")]')->text(), 'password.mail_needed translation required');
        $form = $crawler->filterXPath('.//form')->first()->form();
        $form['change_password_form[plainPassword][first]'] = 'min';
        $form['change_password_form[plainPassword][second]'] = 'min';
        $crawler = $client->submit($form);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(3, $crawler->filterXPath('.//span[contains(@class, "form-error-message")]'), 'The form should display an error message');
        $this->assertEquals($translator->trans('password.min_characters', ['{{ limit }}' => 6], 'validators'), $crawler->filterXPath('.//span[contains(@class, "form-error-message")]')->last()->text(), 'password.min_characters translation required');
        $this->assertEquals($translator->trans('password.one_letter_one_number', [], 'validators'), $crawler->filterXPath('.//span[contains(@class, "form-error-message")]')->first()->text(), 'password.one_letter_one_number translation required');
        $this->assertEquals($translator->trans('password.one_special_letter', [], 'validators'), $crawler->filterXPath('.//span[contains(@class, "form-error-message")]')->eq(1)->text(), 'password.one_special_letter translation required');
        $form = $crawler->filterXPath('.//form')->first()->form();
        $form['change_password_form[plainPassword][first]'] = '12345678a,';
        $crawler = $client->submit($form);
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filterXPath('.//span[contains(@class, "form-error-message")]'), 'The form should display an error message');
        $this->assertEquals($translator->trans('password.password_match', [], 'validators'), $crawler->filterXPath('.//span[contains(@class, "form-error-message")]')->text(), 'password.password_match translation required');

        // Reset success
        $form = $crawler->filterXPath('.//form')->first()->form();
        $form['change_password_form[plainPassword][first]'] = 'aaaBBB111###';
        $form['change_password_form[plainPassword][second]'] = 'aaaBBB111###';
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filterXPath('.//title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/login'));
        $crawler = $client->followRedirect();
        $this->assertCount(1, $crawler->filterXPath('.//div[contains(@class, "alert-success")]'), 'The form should display an success message');
        $this->assertStringContainsString(
            $translator->trans('reset_password.reset.success', [], 'loot'),
            $crawler->filterXPath('.//div[contains(@class, "alert-success")]')->text(),
            'Flash message success expected with translation reset_password.reset.success'
        );

        /*
         * Test again the reset link. Expect a flash message error
         */
        $client->request(Request::METHOD_GET, $reset_url);
        // Follow redirection after token saved in session
        $this->assertTrue($client->getResponse()->isRedirect('/reset-password/reset'));
        $client->followRedirect();
        // Reset invalid, so redirect to the request page
        $this->assertTrue($client->getResponse()->isRedirect('/reset-password'));
        $crawler = $client->followRedirect();
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filterXPath('.//div[contains(@class, "alert-danger")]'), 'The throttling should add a flash message');
        // Message should be a specific translation
        $this->assertStringContainsString(
            $translator->trans(ResetPasswordExceptionInterface::MESSAGE_PROBLEM_VALIDATE, [], 'ResetPasswordBundle'),
            $crawler->filterXPath('.//div[contains(@class, "alert-danger")]')->text(),
            'Flash message danger expected with translation of the ResetPasswordBundle catalog'
        );
    }
}

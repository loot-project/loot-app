<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 14/01/20
 * Time: 11:11.
 */

namespace App\Tests\Functionnals;

use App\Entity\Budget;
use App\Entity\User;
use App\Test\Entity\BudgetTestTrait;
use App\Test\Entity\Loader\BudgetLoader;
use App\Test\Entity\UserTestTrait;
use App\Test\LoginTestCaseTrait;
use App\Test\SessionHelperTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AccountControllerTest extends WebTestCase
{
    use LoginTestCaseTrait;
    use UserTestTrait;
    use BudgetTestTrait;

    public function testCreateAndEditUser(): void
    {
        $client = static::createClient();
        $this->logIn($client, 'root');

        // Display list
        $crawler = $client->request(Request::METHOD_GET, '/account');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(0, $crawler->filter('#user-list li'));

        // go to create form
        $crawler = $client->click($crawler->filter('a[href="/account/create"]')->first()->link());
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        // Test validation
        $form = $crawler->filter('form button')->first()->form();
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('label[for="form_email"]:contains("not be blank")'));

        $form = $crawler->filter('form button')->first()->form();
        $form['form[email]'] = 'root';
        $crawler = $client->submit($form);
        $this->assertCount(1, $crawler->filter('label[for="form_email"]:contains("not a valid email")'));

        // Get Get refreshed session for next request and test
        $session = $client->getRequest()->getSession();
        // Create user with only email
        $form = $crawler->filter('form button')->first()->form();
        $form['form[email]'] = 'root@localhost.com';
        $form['form[send_passsword]'] = false;
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/account'));
        $this->assertContains('success', $session->getFlashBag()->keys());
        $this->assertStringContainsString('Contributor root@localhost.com saved', current($session->getFlashBag()->get('success')));

        // Get refreshed session for next request and test
        $session = $client->getRequest()->getSession();
        // Create user with complete name without send password
        $crawler = $client->request('get', '/account/create');
        $form = $crawler->filter('form button')->first()->form();
        $form['form[email]'] = 'root2@localhost.com';
        $form['form[firstname]'] = 'root';
        $form['form[lastname]'] = 'dupont';
        $form['form[send_passsword]'] = false;
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/account'));
        $this->assertContains('success', $session->getFlashBag()->keys());
        $this->assertStringContainsString('Contributor root2@localhost.com saved', current($session->getFlashBag()->get('success')));
        $this->assertEmailCount(0);

        // Get refreshed session for next request and test
        $session = $client->getRequest()->getSession();
        // Create user with complete name with send password and without custom message
        $crawler = $client->request('get', '/account/create');
        $form = $crawler->filter('form button')->first()->form();
        $form['form[email]'] = 'root3@localhost.com';
        $form['form[firstname]'] = 'root';
        $form['form[lastname]'] = 'dupont2';
        $form['form[send_passsword]'] = true;
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/account'));
        $this->assertContains('success', $session->getFlashBag()->keys());
        $this->assertEmailCount(1);
        $sentMail = $this->getMailerMessage(0);
        $this->assertEmailHeaderSame($sentMail, 'To', 'root3@localhost.com');
        // TODO check if password is in mail body ?

        // Get refreshed session for next request and test
        $session = $client->getRequest()->getSession();
        // Create user with complete name with send password and custom message
        $crawler = $client->request('get', '/account/create');
        $form = $crawler->filter('form button')->first()->form();
        $form['form[email]'] = 'root4@localhost.com';
        $form['form[firstname]'] = 'root';
        $form['form[lastname]'] = 'dupont3';
        $form['form[send_passsword]'] = true;
        $form['form[custom_message]'] = "This is a\ncustom message";
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/account'));
        $this->assertContains('success', $session->getFlashBag()->keys());
        $this->assertEmailCount(1);
        $sentMail = $this->getMailerMessage(0);
        $this->assertEmailHeaderSame($sentMail, 'To', 'root4@localhost.com');
        $this->assertEmailHtmlBodyContains($sentMail, "<p>This is a<br />\ncustom message</p>");
        $this->assertEmailTextBodyContains($sentMail, "This is a\ncustom message");

        // Check users
        $users = $client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(User::class)
            ->findBy([], ['name' => 'asc']);
        $this->assertCount(4, $users);
        $this->assertEquals('root@localhost.com', $users[3]->getName());
        $this->assertEquals('Root DUPONT3', $users[2]->getName());
        $this->assertEquals('Root DUPONT2', $users[1]->getName());
        $this->assertEquals('Root DUPONT', $users[0]->getName());

        // Test create existing user
        $crawler = $client->request('get', '/account/create');
        $form = $crawler->filter('form button')->first()->form();
        $form['form[email]'] = 'root2@localhost.com';
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertCount(1, $crawler->filter('body .alert:contains("already")'));

        // Get refreshed session for next request and test
        $session = $client->getRequest()->getSession();
        // Test change all user information
        $user = current($users);
        $current_password = current($users)->getPassword();
        $crawler = $client->request('get', '/account/'.$user->getId().'/edit');
        $form = $crawler->filter('form button')->first()->form();
        $form['form[email]'] = 'newmail@localhost.com';
        $form['form[firstname]'] = 'newroot';
        $form['form[lastname]'] = 'newnew';
        $form['form[new_password]'] = '1';
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/account'));
        $this->assertContains('success', $session->getFlashBag()->keys());

        $user = $client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(User::class)->find($user->getId());
        $this->assertNotEquals($current_password, $user->getPassword(), 'Expect password change');
        $this->assertEquals('newmail@localhost.com', $user->getEmail(), 'Expect mail change');
        $this->assertEquals('newroot', $user->getFirstname(), 'Expect firstname change');
        $this->assertEquals('newnew', $user->getLastname(), 'Expect lastname change');

        // Get refreshed session for next request and test
        $session = $client->getRequest()->getSession();
        // Test change only firstname
        $current_password = $user->getPassword();
        $crawler = $client->request('get', '/account/'.$user->getId().'/edit');
        $form = $crawler->filter('form button')->first()->form();
        $form['form[firstname]'] = 'pierrot';
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/account'));
        $this->assertContains('success', $session->getFlashBag()->keys());

        $user = $client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(User::class)->find($user->getId());
        $this->assertEquals($current_password, $user->getPassword(), 'Expect password no change');
        $this->assertEquals('pierrot', $user->getFirstname(), 'Expect firstname change');
    }

    public function testListContributor(): void
    {
        $client = static::createClient();

        // Display list
        $this->logIn($client);
        $crawler = $client->request(Request::METHOD_GET, '/account');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('#user-list li'));
    }

    public function testMyAccountHomePage(): void
    {
        $client = static::createClient();

        // Deny access if not logged as user
        $crawler = $client->request(Request::METHOD_GET, '/account/me/');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_FOUND, $client->getResponse()->getStatusCode(), 'Expect login redirection for /account/me/');
        $this->assertStringEndsWith('/login', $client->getResponse()->headers->get('Location'));
        // Deny access if logged as root
        $this->logIn($client, 'root');
        $crawler = $client->request(Request::METHOD_GET, '/account/me/');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode(), 'Expect root denied access for /account/me');

        /*
         * Log as user
         */
        $this->logIn($client);
        $crawler = $client->request(Request::METHOD_GET, '/account/me/');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function testMyAccountEditMail(): void
    {
        $client = static::createClient();

        // Deny access if not logged as user
        $crawler = $client->request(Request::METHOD_GET, '/account/me/edit-mail');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_FOUND, $client->getResponse()->getStatusCode(), 'Expect redirection for /account/me/edit-mail');
        $this->assertStringEndsWith('/login', $client->getResponse()->headers->get('Location'));

        // Deny access if logged as root
        $this->logIn($client, 'root');
        $crawler = $client->request(Request::METHOD_GET, '/account/me/edit-mail');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode(), 'Expect root denied access for /account/me/edit-mail');

        // log as user
        $this->logIn($client);
        $crawler = $client->request(Request::METHOD_GET, '/account/me/edit-mail');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isSuccessful());

        // Test validate empty form : email required
        $form = $crawler->filter('form button')->first()->form();
        $form['form[email]'] = '';
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('label .invalid-feedback'));
        $this->assertCount(1, $crawler->filter('label[for="form_email"] .invalid-feedback'));

        // Test validate bad email
        $form = $crawler->filter('form button')->first()->form();
        $form['form[email]'] = 'truc';
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('label .invalid-feedback'));
        $this->assertCount(1, $crawler->filter('label[for="form_email"] .invalid-feedback'));

        // Test valid modification
        $form = $crawler->filter('form button')->first()->form();
        $form['form[email]'] = 'newmail@localhost.com';
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/account/me/edit-mail'), 'Redirect to /account/me/edit-mail expected after valid email modification');

        // Verify modification in database
        $user = $client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(User::class)->findOneBy(['email' => 'newmail@localhost.com']);
        $this->assertNotNull($user, 'User not found with new email after modification');
    }

    public function testMyAccountEditPassword(): void
    {
        $client = static::createClient();

        // Deny access if not logged as user
        $crawler = $client->request(Request::METHOD_GET, '/account/me/edit-password');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_FOUND, $client->getResponse()->getStatusCode(), 'Expect redirection for /account/me/edit-password');
        $this->assertStringEndsWith('/login', $client->getResponse()->headers->get('Location'));
        // Deny access if logged as root
        $this->logIn($client, 'root');
        $crawler = $client->request(Request::METHOD_GET, '/account/me/edit-password');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode(), 'Expect root denied access for /account/me/edit-password');

        // log as user
        $user = $this->logIn($client);
        $crawler = $client->request(Request::METHOD_GET, '/account/me/edit-password');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isSuccessful());

        // Test validate empty form : old and new password required
        $form = $crawler->filter('form button')->first()->form();
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(2, $crawler->filter('label .invalid-feedback'));
        $this->assertCount(1, $crawler->filter('label[for="form_new_password"] .invalid-feedback'));
        $this->assertCount(1, $crawler->filter('label[for="form_old_password"] .invalid-feedback'));

        // Test validate new password email
        $form = $crawler->filter('form button')->first()->form();
        $form['form[old_password]'] = 'password';
        $form['form[new_password]'] = '12';
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('label .invalid-feedback'));
        $this->assertCount(1, $crawler->filter('label[for="form_new_password"] .invalid-feedback'));
        $form = $crawler->filter('form button')->first()->form();
        $form['form[old_password]'] = 'password';
        $form['form[new_password]'] = '1245678';
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('label .invalid-feedback'));
        $this->assertCount(1, $crawler->filter('label[for="form_new_password"] .invalid-feedback'));
        $form = $crawler->filter('form button')->first()->form();
        $form['form[old_password]'] = 'password';
        $form['form[new_password]'] = '123456Za';
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('label .invalid-feedback'));
        $this->assertCount(1, $crawler->filter('label[for="form_new_password"] .invalid-feedback'));

        // Test bad password
        $form = $crawler->filter('form button')->first()->form();
        $form['form[old_password]'] = 'password';
        $form['form[new_password]'] = '12345$Za';
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('label .invalid-feedback'));
        $this->assertCount(1, $crawler->filter('label[for="form_old_password"] .invalid-feedback'));

        // Test valid modification
        $form = $crawler->filter('form button')->first()->form();
        $form['form[old_password]'] = 'p@ssword';
        $form['form[new_password]'] = '12345$Za';
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/account/me/'), 'Redirect to /account/me expected after change password');
        $this->assertTrue($client->getResponse()->isRedirect('/account/me/'), 'Redirect to /account/me/expected after change password');

        // Verify modification in database
        $user = $client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(User::class)->findOneBy(['email' => $user->getEmail()]);
        $this->assertEquals($user->getPassword(), '12345$Za');
    }

    public function testAccountSimpleView(): void
    {
        $client = static::createClient();
        $entityManager = $client->getContainer()->get('doctrine.orm.default_entity_manager');

        // Login required
        $crawler = $client->request(Request::METHOD_GET, '/account/1/view');
        $this->assertEquals(Response::HTTP_FOUND, $client->getResponse()->getStatusCode(), 'Expect redirection for /account/me');
        $this->assertStringEndsWith('/login', $client->getResponse()->headers->get('Location'));

        // Login and create user
        $user = $this->logIn($client);
        $crawler = $client->request(Request::METHOD_GET, '/account/'.$user->getId().'/view');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode(), 'Expect 200 for /account/'.$user->getId().'/view');
    }

    public function testAccountFullView(): void
    {
        $client = static::createClient();
        $entityManager = $client->getContainer()->get('doctrine.orm.default_entity_manager');

        // Login and fill user
        $user = $this->logIn($client);
        // Set firstname
        $user->setFirstname('pierre');
        $user->setLastname('dupont');
        $entityManager->flush();

        /*
         * Test view my profile
         */
        $crawler = $client->request(Request::METHOD_GET, '/account/'.$user->getId().'/view');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode(), 'Expect 200 for /account/'.$user->getId().'/view');
        $this->assertCount(1, $crawler->filter('body:contains("Pierre")'));
        $this->assertCount(1, $crawler->filter('.btn-edit'));
        $this->assertCount(1, $crawler->filter('.btn-delete'));

        // With budget
        $budgets_loader = new BudgetLoader();
        $budgets = $budgets_loader->load($entityManager);
        /** @var Budget $budget */
        $budget = current($budgets);
        // Join budget
        $user->addBudget($budget);
        $budget->addContributor($user);
        $entityManager->flush();

        $client->getContainer()->get('doctrine.orm.default_entity_manager')->flush();
        $crawler = $client->request(Request::METHOD_GET, '/account/'.$user->getId().'/view');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode(), 'Expect 200 for /account/'.$user->getId().'/view');
        $this->assertCount(1, $crawler->filter('.card-budgets li'));
        $this->assertCount(1, $crawler->filter('.btn-edit'));
        $this->assertCount(0, $crawler->filter('.btn-delete'));

        /*
         * Test view other than me
         */
        $user_paul = new User();
        $user_paul->setEmail('paul@localhost');
        $user_paul->setName($user_paul->getEmail());
        $user_paul->setPassword('p@ssword');
        $entityManager->persist($user_paul);
        $entityManager->flush();

        $client->getContainer()->get('doctrine.orm.default_entity_manager')->flush();
        $crawler = $client->request(Request::METHOD_GET, '/account/'.$user_paul->getId().'/view');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode(), 'Expect 200 for /account/'.$user->getId().'/view');
        $this->assertCount(0, $crawler->filter('.btn-edit'));
        $this->assertCount(0, $crawler->filter('.btn-delete'));
    }

    public function testRetributionList(): void
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        // Load data
        $container->get('fidry_alice_data_fixtures.loader.doctrine')->load(
            [__DIR__.'/../../fixtures/tests/acccounts/account_with_retributions.yaml']
        );

        // Log as user1
        $user = $this->logIn($client, 'user1');

        // Search budget with retributions
        $crawler = $client->request(Request::METHOD_GET, '/account/'.$user->getId().'/view');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('.retribution-list .retribution'));
    }

    public function testDeleteView(): void
    {
        $client = static::createClient();
        $entityManager = $client->getContainer()->get('doctrine.orm.default_entity_manager');

        // Login and fill user
        $paul = $this->createUser('paul', $entityManager);

        /*
         * Test delete view from root login
         */
        $this->logIn($client, 'root');
        $client->getContainer()->get('doctrine.orm.default_entity_manager')->flush();
        $crawler = $client->request(Request::METHOD_GET, '/account/'.$paul->getId().'/delete-view');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode(), 'Expect 200 for /account/'.$paul->getId().'/view');
        $this->assertCount(1, $crawler->filter('.btn-delete'));

        /*
         * Test delete my profile
         */
        $this->logIn($client, 'paul');
        $crawler = $client->request(Request::METHOD_GET, '/account/'.$paul->getId().'/delete-view');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode(), 'Expect 200 for /account/'.$paul->getId().'/view');
        $this->assertCount(1, $crawler->filter('.btn-delete'));

        // With budget
        $me = $this->createUser('me', $entityManager);
        $budgets_loader = new BudgetLoader();
        $budgets = $budgets_loader->load($entityManager);
        $budget = current($budgets);
        $this->joinBudget($me, $budget, $entityManager);

        $this->logIn($client, 'me');
        $crawler = $client->request(Request::METHOD_GET, '/account/'.$paul->getId().'/delete-view');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode(), 'Expect 200 for /account/'.$paul->getId().'/view');
        $this->assertCount(0, $crawler->filter('.btn-delete'));

        /*
         * Test delete view other than me
         */
        $pierre = $this->createUser('pierre', $entityManager);
        $client->getContainer()->get('doctrine.orm.default_entity_manager')->flush();
        $crawler = $client->request(Request::METHOD_GET, '/account/'.$pierre->getId().'/delete-view');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode(), 'Expect 200 for /account/'.$pierre->getId().'/view');
        $this->assertCount(0, $crawler->filter('.btn-delete'));
    }

    public function testDeleteAccount(): void
    {
        $client = static::createClient();
        $entityManager = $client->getContainer()->get('doctrine.orm.default_entity_manager');

        $user = new User();
        $user->setEmail('paul@localhost');
        $user->setName($user->getEmail());
        $user->setPassword('p@ssword');
        $entityManager->persist($user);
        $entityManager->flush();

        /*
         * Test grant access
         */
        // Anonymous
        $crawler = $client->request(Request::METHOD_GET, '/account/'.$user->getId().'/delete');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertStringEndsWith('/login', $client->getResponse()->headers->get('Location'));
        // Delete other user
        $this->logIn($client);
        $crawler = $client->request(Request::METHOD_GET, '/account/'.$user->getId().'/delete');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode(), 'Expect denied access delete account not of logged user');

        /*
         * Delete
         */
        // Delete as root
        $this->assertNotNull($entityManager->getRepository(User::class)->find($user->getId()));
        $this->logIn($client, 'root');
        $crawler = $client->request(Request::METHOD_GET, '/account/'.$user->getId().'/delete');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirection());
        $this->assertStringEndsWith('/account', $client->getResponse()->headers->get('Location'));
        // Get refreshed session for next request and test
        $session = $client->getRequest()->getSession();
        $this->assertContains('success', $session->getFlashBag()->keys());
        $this->assertStringContainsString('deleted', current($session->getFlashBag()->get('success')));
        $entityManager->clear();
        $this->assertNull($entityManager->getRepository(User::class)->find($user->getId()));
    }

    public function testDeleteMyAccount(): void
    {
        // $this->markTestIncomplete("");

        $client = static::createClient();
        $entityManager = $client->getContainer()->get('doctrine.orm.default_entity_manager');

        $user_logged = $this->logIn($client);
        $user_id = $user_logged->getId();

        // Delete me
        $this->assertNotNull($entityManager->getRepository(User::class)->find($user_logged->getId()));
        $crawler = $client->request(Request::METHOD_GET, '/account/'.$user_logged->getId().'/delete');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirection());
        $this->assertStringEndsWith('/login', $client->getResponse()->headers->get('Location'));
        // Get session and test
        $session = $client->getRequest()->getSession();
        $this->assertContains('success', $session->getFlashBag()->keys());
        $this->assertStringContainsString('deleted', current($session->getFlashBag()->get('success')));

        // Follow to check if no error after delete a logged in user
        $crawler = $client->followRedirect();
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        // Check user deleted
        $entityManager->clear();
        $this->assertNull($entityManager->getRepository(User::class)->find($user_id));
    }
}

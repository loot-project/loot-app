<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 14/01/20
 * Time: 11:11.
 */

namespace App\Tests\Functionnals;

use App\Test\LoginTestCaseTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SecurityControllerTest extends WebTestCase
{
    use LoginTestCaseTrait;

    public function testRootConnexion(): void
    {
        $client = static::createClient();

        // Request form
        $crawler = $client->request(Request::METHOD_GET, '/login');
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        // Submit form
        $form = $crawler->filter('#login-screen form button')->first()->form();
        $form['_username'] = 'root';
        $form['_password'] = 'badpassword';
        $client->submit($form);
        $this->assertTrue($client->getResponse()->isRedirect('http://localhost/login'));
        $crawler = $client->followRedirect();
        $this->assertGreaterThan(
            0,
            $crawler->filter('form:contains("failed")')->count()
        );

        // Right connexion
        $form = $crawler->filter('#login-screen form button')->first()->form();
        $form['_username'] = 'root';
        $form['_password'] = 'root';
        $client->submit($form);
        $this->assertTrue($client->getResponse()->isRedirect('http://localhost/app/dashboard'));
    }

    /**
     * Test if root access to dashboard.
     */
    public function testRootAccessLimitedRoute(): void
    {
        $client = static::createClient();
        $this->logIn($client, 'root');

        // Request form
        $client->request(Request::METHOD_GET, '/app/dashboard');
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 14/01/20
 * Time: 11:11.
 */

namespace App\Tests\Functionnals;

use App\Test\Entity\BudgetTestTrait;
use App\Test\Entity\UserTestTrait;
use App\Test\LoginTestCaseTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RetributionControllerTest extends WebTestCase
{
    use LoginTestCaseTrait;
    use BudgetTestTrait;
    use UserTestTrait;

    public function testAccessDeniedAnonymousUser(): void
    {
        $client = static::createClient();

        $crawler = $client->request(Request::METHOD_GET, '/retribution');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_FOUND, $client->getResponse()->getStatusCode(), 'Redirection expected');
        $this->assertStringEndsWith('/login', $client->getResponse()->headers->get('Location'));
    }

    public function testRootView(): void
    {
        $client = static::createClient();

        $this->logIn($client, 'root');

        $crawler = $client->request(Request::METHOD_GET, '/retribution');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isSuccessful(), 'Successful code expected');
    }

    public function testRetributionsListViewByContributor(): void
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        // Log as user1
        $this->logIn($client);

        $crawler = $client->request(Request::METHOD_GET, '/retribution');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        // Load data
        $container->get('fidry_alice_data_fixtures.loader.doctrine')->load(
            [__DIR__.'/../../fixtures/tests/retributions/retributions_list_view.yaml']
        );

        // Log as user1
        $this->logIn($client, 'user1');

        $crawler = $client->request(Request::METHOD_GET, '/retribution');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('.retribution-list .retribution'));
    }
}

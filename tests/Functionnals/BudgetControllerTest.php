<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 14/01/20
 * Time: 11:11.
 */

namespace App\Tests\Functionnals;

use App\Entity\Activity;
use App\Entity\Budget;
use App\Entity\Contribution;
use App\Entity\Manager\BudgetManager;
use App\Entity\User;
use App\Test\Entity\BudgetTestTrait;
use App\Test\Entity\Loader\BudgetLoader;
use App\Test\Entity\UserTestTrait;
use App\Test\LoginTestCaseTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BudgetControllerTest extends WebTestCase
{
    use LoginTestCaseTrait;
    use BudgetTestTrait;
    use UserTestTrait;

    public function testRootDeniedAccess(): void
    {
        $client = static::createClient();

        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        $this->logIn($client, 'root');

        // Budget create
        $crawler = $client->request(Request::METHOD_GET, '/budget/create');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode(), 'Expect root denied for /budget/create');

        // Load budget and a contribution
        $budgets_loader = new BudgetLoader();
        $budgets = $budgets_loader->load($entityManager);
        /** @var Budget $budget */
        $budget = current($budgets);

        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/join');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode(), 'Expect root denied for /budget/join');
    }

    public function testBudgetList(): void
    {
        $client = static::createClient();
        $this->logIn($client);

        // Start from budget
        $crawler = $client->request(Request::METHOD_GET, '/budget');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        $this->assertCount(0, $crawler->filter('.budget-item'));
    }

    protected function _makeBudgetFormValidationAssertion(KernelBrowser $client, $form)
    {
        $form['budget_form[unit]'] = 'hour';
        $form['budget_form[title]'] = '';
        $form['budget_form[unitSymbol]'] = '';
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(2, $crawler->filter('label .invalid-feedback'));
        $this->assertCount(1, $crawler->filter('label[for="budget_form_title"] .invalid-feedback'));
        $this->assertCount(1, $crawler->filter('label[for="budget_form_unitSymbol"] .invalid-feedback'));

        // test unit required
        $form = $crawler->filter('form button')->first()->form();
        $form['budget_form[unitSymbol]'] = 'h';
        $form['budget_form[title]'] = 'Budget title';
        $form['budget_form[unit]'] = '';
        $crawler = $client->submit($form);
        $this->assertCount(1, $crawler->filter('label .invalid-feedback'));
        $this->assertCount(1, $crawler->filter('label[for="budget_form_unit"] .invalid-feedback'));

        // Test unit symbol with more than 6 letters
        $form = $crawler->filter('form button')->first()->form();
        $form['budget_form[unitSymbol]'] = 'hour ttc';
        $form['budget_form[title]'] = 'Budget title';
        $form['budget_form[unit]'] = 'hour ttc';
        $crawler = $client->submit($form);
        $this->assertCount(1, $crawler->filter('label .invalid-feedback'));
        $this->assertCount(1, $crawler->filter('label[for="budget_form_unitSymbol"] .invalid-feedback'));

        // Test unit missing with amount set
        $form = $crawler->filter('form button')->first()->form();
        $form['budget_form[amount]'] = '1000';
        $form['budget_form[unitSymbol]'] = '';
        $form['budget_form[unit]'] = '';
        $form['budget_form[title]'] = 'Budget title';
        $crawler = $client->submit($form);
        $this->assertCount(2, $crawler->filter('label .invalid-feedback'));
        $this->assertCount(1, $crawler->filter('label[for="budget_form_unitSymbol"] .invalid-feedback'));
        $this->assertCount(1, $crawler->filter('label[for="budget_form_unit"] .invalid-feedback'));
    }

    public function testCreateAndListBudget(): void
    {
        $client = static::createClient();
        $this->logIn($client);

        // Add user used after for create budget with referent
        $em = $client->getContainer()->get('doctrine')->getManager();
        $alonzo = $this->createUser('alonzo', $client->getContainer()->get('doctrine')->getManager());

        $activityIds[0] = $this->createActivity('developpement', $em)->getId();
        $activityIds[1] = $this->createActivity('communications', $em)->getId();

        // Start from budget
        $crawler = $client->request(Request::METHOD_GET, '/budget/create');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        /*
         * Test validation
         */
        // Test title and unit symbol missing
        $form = $crawler->filter('form button')->first()->form();
        $this->_makeBudgetFormValidationAssertion($client, $form);

        /*
         * Test full creation
         */
        $form = $crawler->filter('form button')->first()->form();
        $form['budget_form[unitSymbol]'] = 'h';
        $form['budget_form[title]'] = 'Budget title';
        $form['budget_form[unit]'] = 'hour';
        $form['budget_form[subtitle]'] = 'Budget subtitle';
        $form['budget_form[rules]'] = 'http://www.rules.org';
        $form['budget_form[tags]'] = 'tag1,tag2';
        $form['budget_form[activities]']->select($activityIds);
        $form['budget_form[referent]'] = $alonzo->getId();
        $form['budget_form[amount]'] = '1000';
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/budget'), 'Html : '.$crawler->html());
        $crawler = $client->followRedirect();
        $this->assertCount(1, $crawler->filter('body:contains("created")'));

        // Check data
        $budgets = $client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(Budget::class)
                        ->findBy([], ['title' => 'asc']);
        $this->assertCount(1, $budgets);
        $this->assertEquals('Budget title', $budgets[0]->getTitle());
        $this->assertEquals('Budget subtitle', $budgets[0]->getSubtitle());
        $this->assertEquals('http://www.rules.org', $budgets[0]->getRules());
        $this->assertEquals('tag1,tag2', $budgets[0]->getTags());
        $this->assertEquals('alonzo@localhost', $budgets[0]->getReferent()->getEmail());
        $this->assertEquals(1000, $budgets[0]->getAmount());
        $this->assertFalse($budgets[0]->isTimetracking());
        $this->assertEquals('hour', $budgets[0]->getUnit());
        $this->assertEquals('h', $budgets[0]->getUnitSymbol());
        $this->assertCount(2, $budgets[0]->getActivities());
        $this->assertContains($budgets[0]->getActivities()[0]->getTitle(), ['communications', 'developpement']);
        $this->assertContains($budgets[0]->getActivities()[1]->getTitle(), ['communications', 'developpement']);

        // Test if user added to budget
        $this->assertCount(1, $budgets[0]->getContributors(), "Created user must be added to budget's contributors");
        $this->assertEquals("user@localhost", $budgets[0]->getContributors()[0]->getEmail(), "Created user must be added to budget's contributors");

        // Display list
        $crawler = $client->request(Request::METHOD_GET, '/budget');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('.budget-list-item'));
    }

    public function testEditBudget(): void
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        // Load budget
        $budgets_loader = new BudgetLoader();
        $budgets = $budgets_loader->load($entityManager);
        $budget = current($budgets);

        // Add user used after for create budget with referent
        $alonzo = $this->createUser('alonzo', $client->getContainer()->get('doctrine')->getManager());

        /*
         * Check access
         */
        // No user
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/edit');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_FOUND, $client->getResponse()->getStatusCode(), 'Expect login redirection for /account/me/');
        $this->assertStringEndsWith('/login', $client->getResponse()->headers->get('Location'));
        // Root user
        $this->logIn($client, 'root');
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/edit');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isForbidden());
        // User not contributor
        $user = $this->logIn($client);
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/edit');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isForbidden());

        /*
         * Test full edit
         * We have to recreate dataset because after each request entitmanager is unstable?
         */
        // Load new budget
        $budgets_loader = new BudgetLoader();
        $budgets = $budgets_loader->load($entityManager);
        $budget = current($budgets);
        // Login and fill user
        $paul = $this->createUser('paul', $entityManager);
        // Add user to budgets
        $this->joinBudget($paul, $budget, $entityManager);
        $this->logIn($client, 'paul');
        // Activities creation (add item in menu)
        $activityIds[0] = $this->createActivity('developpement', $entityManager)->getId();
        $activityIds[1] = $this->createActivity('communications', $entityManager)->getId();

        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/edit');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('form input[name="budget_form[unitSymbol]"]'));

        // Check validation
        $form = $crawler->filter('form button')->first()->form();
        $this->_makeBudgetFormValidationAssertion($client, $form);

        // Change value and check data
        // Add an html option to the select having the class .select-activities
        $domDocument = $crawler->getNode(0)->parentNode;
        $option = $domDocument->createElement('option', 'test');
        $option->setAttribute('value', '_test');
        $select = $crawler->filter('.select-activities option')->getNode(0);
        $select->parentNode->insertBefore($option, $select->nextSibling);

        // Get refreshed session for next request and test
        $session = $client->getRequest()->getSession();
        $form = $crawler->filter('form button')->first()->form();
        $form['budget_form[unitSymbol]'] = 'b';
        $form['budget_form[title]'] = 'Budget title 2.0';
        $form['budget_form[unit]'] = 'beko';
        $form['budget_form[subtitle]'] = 'Budget subtitle 2.0';
        $form['budget_form[rules]'] = 'http://rules.org';
        $form['budget_form[tags]'] = 'tag1,tag3';
        // Add new activity and selection ony only one already created
        $form['budget_form[activities]']->select([$activityIds[0], '_test']);
        $form['budget_form[referent]'] = $alonzo->getId();
        $form['budget_form[amount]'] = '500';
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/budget'));
        $this->assertContains('success', $session->getFlashBag()->keys());
        $this->assertStringContainsString('saved', current($session->getFlashBag()->get('success')));

        // Check data
        $budgets = $client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(Budget::class)
                          ->findBy(['slug' => $budget->getSlug()], ['title' => 'asc']);
        $this->assertCount(1, $budgets);
        $this->assertEquals('Budget title 2.0', $budgets[0]->getTitle());
        $this->assertEquals('Budget subtitle 2.0', $budgets[0]->getSubtitle());
        $this->assertEquals('http://rules.org', $budgets[0]->getRules());
        $this->assertEquals('tag1,tag3', $budgets[0]->getTags());
        $this->assertEquals('alonzo@localhost', $budgets[0]->getReferent()->getEmail());
        $this->assertEquals(500, $budgets[0]->getAmount());
        $this->assertTrue($budgets[0]->isTimetracking());
        $this->assertEquals('beko', $budgets[0]->getUnit());
        $this->assertEquals('b', $budgets[0]->getUnitSymbol());
        $this->assertCount(2, $budgets[0]->getActivities());
        $this->assertEquals('developpement', $budgets[0]->getActivities()[0]->getTitle());
        $this->assertEquals('test', $budgets[0]->getActivities()[1]->getTitle());
        $this->assertCount(0, $entityManager->getRepository(Activity::class)->findBy(['title' => 'communications']), 'Activity not used must be deleted');

        // check

        /*
         * Test edit budget having already contributions
         */
        // Add one contribution
        $contribution = new Contribution();
        $contribution->setBudget($budget);
        $contribution->setAmount(10);
        $contribution->setDescription('First contribution');
        $contribution->setDoned(new \DateTime());
        $contribution->setDuration(1);
        $contribution->setContributor($paul);
        $entityManager->persist($contribution);
        $entityManager->flush();

        // unselect a activity et add new it
        $activityIds[1] = $this->createActivity('secretariat', $entityManager)->getId();

        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/edit');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(0, $crawler->filter('form input[name="budget_form[unitSymbol]"]'));
        $this->assertCount(0, $crawler->filter('form input[name="budget_form[unit]"]'));

        // Get refreshed session for next request and test
        $session = $client->getRequest()->getSession();
        // Change value and check data
        $form = $crawler->filter('form button')->first()->form();
        $form['budget_form[title]'] = 'Budget title 2.1';
        $form['budget_form[subtitle]'] = 'Budget subtitle 2.1';
        $form['budget_form[amount]'] = '650';
        $form['budget_form[activities]']->select($activityIds);
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/budget'));
        $this->assertContains('success', $session->getFlashBag()->keys());
        $this->assertStringContainsString('saved', current($session->getFlashBag()->get('success')));
        $budgets = $client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(Budget::class)
                          ->findBy(['slug' => $budget->getSlug()], ['title' => 'asc']);
        $this->assertCount(1, $budgets);
        $this->assertEquals('Budget title 2.1', $budgets[0]->getTitle());
        $this->assertEquals('Budget subtitle 2.1', $budgets[0]->getSubtitle());
        $this->assertEquals(650, $budgets[0]->getAmount());
        $this->assertCount(2, $budgets[0]->getActivities());
        $this->assertEquals('developpement', $budgets[0]->getActivities()[0]->getTitle());
        $this->assertEquals('secretariat', $budgets[0]->getActivities()[2]->getTitle());

        // The activity not used must be deleted
        $activities = $entityManager->getRepository(Activity::class)
                          ->findBy([], ['title' => 'asc']);
        $this->assertCount(2, $activities);
    }

    public function testJoinAndQuit(): void
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        $this->logIn($client);

        // Load budget
        $budgets_loader = new BudgetLoader();
        $budgets = $budgets_loader->load($entityManager);
        $budget = current($budgets);

        // Join
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/join');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/budget'));
        $crawler = $client->followRedirect();
        $this->assertCount(1, $crawler->filter('body .alert:contains("Yes")'));

        // Already join
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/join');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/budget'));
        $crawler = $client->followRedirect();
        $this->assertCount(1, $crawler->filter('body .alert:contains("You have already joined the budget")'));

        // Leave
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/leave');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/budget'));
        $crawler = $client->followRedirect();
        $this->assertCount(1, $crawler->filter('body .alert:contains("You have leaved")'));
    }

    public function testQuitDenied(): void
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        $user = $this->logIn($client);

        /*
         * DATASET
         */
        // Load budget and a contribution
        $budgets_loader = new BudgetLoader();
        $budgets = $budgets_loader->load($entityManager);
        /** @var Budget $budget */
        $budget = current($budgets);

        // Join budget
        $user->addBudget($budget);
        $budget->addContributor($user);

        // Add one contribution
        $contribution = new Contribution();
        $contribution->setBudget($budget);
        $contribution->setAmount(10);
        $contribution->setDescription('First contribution');
        $contribution->setDoned(new \DateTime());
        $contribution->setDuration(1);
        $contribution->setContributor($user);
        $entityManager->persist($contribution);
        $entityManager->flush();

        /*
         * TEST DENIED
         */
        // Leave
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/leave');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/budget'));
        $crawler = $client->followRedirect();
        $this->assertCount(1, $crawler->filter('body .alert:contains("You can not leave")'),
            $crawler->filter('body')->text());

        /*
         * TEST OK IF ALL CLAIM
         */
        // Data set
        $user = $this->createUser('paul', $entityManager);
        // Load budget and a contribution
        $budgets_loader = new BudgetLoader();
        $budgets = $budgets_loader->load($entityManager);
        /** @var Budget $budget */
        $budget = current($budgets);

        // Join budget
        $user->addBudget($budget);
        $budget->addContributor($user);

        // Add one contribution
        $contribution = new Contribution();
        $contribution->setBudget($budget);
        $contribution->setAmount(10);
        $contribution->setDescription('First contribution');
        $contribution->setDoned(new \DateTime());
        $contribution->setDuration(1);
        $contribution->setContributor($user);
        $entityManager->persist($contribution);
        $entityManager->flush();

        // Claim et leave
        $retribution = $client->getContainer()->get(BudgetManager::class)->claim($user, [$contribution]);
        $entityManager->persist($retribution);
        $entityManager->flush();

        $this->logIn($client, 'paul');
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/leave');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/budget'));
        $crawler = $client->followRedirect();
        $this->assertCount(1, $crawler->filter('body .alert:contains("You have leaved")'));
    }

    /**
     * Test log contribution.
     *
     *  - Seul un contributeur du budget peut saisir
     * - Le contributeur peut déclarer "Je ne souhaite pas de rétribution pour cette contribution"
     * - Proposer de saisir l'heure uniquement si demandé dans la paramétrage du budget
     * - La barre d'utilisation du budget évolue en fonction des saisies
     */
    public function testLogView(): void
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        $user = $this->logIn($client);

        // Load budget
        $budget_loader = new BudgetLoader();
        $budget_loader->addBudgetWithoutTimeTracking('Just log budget');
        $budget_loader->addBudgetWithRules();
        $budgets = $budget_loader->load($entityManager);
        /** @var Budget $budget */
        $budget_tracked_with_amount = current($budgets);
        $budget_no_tracked_no_amount = next($budgets);
        $budget_with_rules = next($budgets);

        // Check access
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget_tracked_with_amount->getSlug().'/log');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isForbidden());

        // Add user to budgets
        $this->joinBudget($user, $budget_tracked_with_amount, $entityManager);
        $this->joinBudget($user, $budget_no_tracked_no_amount, $entityManager);
        $this->joinBudget($user, $budget_with_rules, $entityManager);

        // Check form composition (timetracking and amount asked or not depends on budget configuration)
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget_tracked_with_amount->getSlug().'/log');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('#contribution_form_amount'));
        $this->assertCount(1, $crawler->filter('#contribution_form_duration'));
        $this->assertCount(0, $crawler->filter('.budget-rules-notice'), 'No rules notice expected');
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget_no_tracked_no_amount->getSlug().'/log');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(0, $crawler->filter('#contribution_form_amount'));
        $this->assertCount(0, $crawler->filter('#contribution_form_duration'));

        // If rules set, display notice
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget_with_rules->getSlug().'/log');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertCount(1, $crawler->filter('.budget-rules-notice'), 'Rules notice expected');
    }

    public function testLogContribution(): void
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        $user = $this->logIn($client);
        // Load budget
        $budget_loader = new BudgetLoader();
        $budgets = $budget_loader->load($entityManager);
        /** @var Budget $budget_tracked_with_amount_and_time_tracking */
        $budget_tracked_with_amount_and_time_tracking = current($budgets);
        // Add user to budgets
        $this->joinBudget($user, $budget_tracked_with_amount_and_time_tracking, $entityManager);

        // Test log with an activity
        // Activities creation (add item in menu)
        $activityIds[0] = $this->createActivity('developpement', $entityManager)->getId();
        $activityIds[1] = $this->createActivity('communications', $entityManager)->getId();
        $budget_tracked_with_amount_and_time_tracking->addActivity($entityManager->getRepository(Activity::class)->find($activityIds[0]));
        $budget_tracked_with_amount_and_time_tracking->addActivity($entityManager->getRepository(Activity::class)->find($activityIds[1]));
        $entityManager->persist($budget_tracked_with_amount_and_time_tracking);
        $entityManager->flush();
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget_tracked_with_amount_and_time_tracking->getSlug().'/log');
        // Get refreshed session for next request and test
        $session = $client->getRequest()->getSession();
        $form = $crawler->filter('form button')->first()->form();
        $form['contribution_form[rewarded]'] = '1';
        $form['contribution_form[amount]'] = '15';
        $form['contribution_form[doned]'] = '2024-05-10';
        $form['contribution_form[duration]'] = '2';
        $form['contribution_form[activities]']->select([$activityIds[0]]);
        $form['contribution_form[description]'] = 'Un truc';
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/budget/'.$budget_tracked_with_amount_and_time_tracking->getSlug().'/log'), 'Redirection to log a new contribution expected');
        $this->assertContains('success', $session->getFlashBag()->keys(), 'Success message expected');
        $this->assertStringContainsString('saved', current($session->getFlashBag()->get('success')), 'Success message with saved expected');

        // Check data
        $contributions = $client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(Contribution::class)
            ->findBy(['budget' => $budget_tracked_with_amount_and_time_tracking]);
        $this->assertCount(1, $contributions, 'One contribution expected');
        $this->assertEquals(15, $contributions[0]->getAmount());
        $this->assertEquals('2024-05-10', $contributions[0]->getDoned()->format('Y-m-d'));
        $this->assertEquals(2, $contributions[0]->getDuration());
        $this->assertEquals('Un truc', $contributions[0]->getDescription());
        $this->assertCount(1, $contributions[0]->getActivities());
        $this->assertEquals('developpement', $contributions[0]->getActivities()[0]->getTitle());
    }

    /**
     * Test claim.
     *
     *  - List logs
     * - Le contributeur peut déclarer "Je ne souhaite pas de rétribution pour cette contribution"
     * - Proposer de saisir l'heure uniquement si demandé dans la paramétrage du budget
     * - La barre d'utilisation du budget évolue en fonction des saisies
     */
    public function testClaim(): void
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        $user = $this->logIn($client);

        // Load based data
        $budget_loader = new BudgetLoader();
        $budgets = $budget_loader->load($entityManager);
        $budget_tracked_with_amount = current($budgets);
        $other_user = $this->addUser('other_user', $entityManager);

        // Add losg for another user
        $this->generateContributions($other_user, $budget_tracked_with_amount, $entityManager);

        // Check access
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget_tracked_with_amount->getSlug().'/claim');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isForbidden());

        // Add user to budgets
        $this->joinBudget($user, $budget_tracked_with_amount, $entityManager);

        // Set referent to budget
        $this->setReferent($this->addUser('budget', $entityManager), $budget_tracked_with_amount, $entityManager);

        // Test claim with no log
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget_tracked_with_amount->getSlug().'/claim');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        // Add logs for logged in user. We have to find entity because after a request, the entity manager looses the sync with database.
        $this->generateContributions(
            $entityManager->getRepository(User::class)->find($user->getId()),
            $entityManager->getRepository(Budget::class)->find($budget_tracked_with_amount->getId()),
            $entityManager);

        // Test claim with 3 log
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget_tracked_with_amount->getSlug().'/claim');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(3, $crawler->filter('.log-list-item'));

        // Claim empty log : redirect to the same page
        $form = $crawler->filter('form button')->first()->form();
        $form['claim-ids'] = '';
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/budget/'.$budget_tracked_with_amount->getSlug().'/claim'));

        // Claim two log
        $crawler = $client->followRedirect();
        $contribution_id_1 = $crawler->filter('.log-list-item [data-id]')->first()->attr('data-id');
        $contribution_id_2 = $crawler->filter('.log-list-item [data-id]')->last()->attr('data-id');
        $form = $crawler->filter('form button')->first()->form();
        $form['claim-ids'] = $contribution_id_1.','.$contribution_id_2;
        $crawler = $client->submit($form);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirection());
        $retribution_id = [];
        preg_match('~/([0-9]+)/claim-view$~', (string) $client->getResponse()->headers->get('Location'), $retribution_id);
        $retribution_id = end($retribution_id);
        $this->assertEmailCount(2);
        $sentMail = $this->getMailerMessage(0);
        $this->assertEmailHeaderSame($sentMail, 'To', 'budget@localhost');
        $sentMail = $this->getMailerMessage(1);
        $this->assertEmailHeaderSame($sentMail, 'To', 'user@localhost');

        // Only 1 log to claim now
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget_tracked_with_amount->getSlug().'/claim');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('.log-list-item'));
    }

    /**
     * Test the claim view.
     *
     * - Forbidden if not a claim of the logged in user
     */
    public function testClaimView(): void
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        $user = $this->logIn($client);

        // Load based data
        $budget_loader = new BudgetLoader();
        $budgets = $budget_loader->load($entityManager);
        $budget_tracked_with_amount = current($budgets);

        // Test claim view of other user : forbidden
        $other_user = $this->addUser('other_user', $entityManager);
        $contributions = $this->generateContributions($other_user, $budget_tracked_with_amount, $entityManager);
        $retribution = $client->getContainer()->get(BudgetManager::class)->claim($other_user, $contributions);
        $entityManager->persist($retribution);
        $entityManager->flush();
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget_tracked_with_amount->getSlug().'/'.$retribution->getId().'/claim-view');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isForbidden());

        // Test claim view of login user
        $this->logIn($client, 'other_user');
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget_tracked_with_amount->getSlug().'/'.$retribution->getId().'/claim-view');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testDeleteAsRoot(): void
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        // Load budget
        $budgets_loader = new BudgetLoader();
        $budgets = $budgets_loader->load($entityManager);
        $budget = current($budgets);

        /*
         * Check access
         */
        // No user
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/delete');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_FOUND, $client->getResponse()->getStatusCode(), 'Expect login redirection for /account/me/');
        $this->assertStringEndsWith('/login', $client->getResponse()->headers->get('Location'));

        /*
         * Check access : User contributor but already one contribution
         */
        // Load new budget
        $budgets_loader = new BudgetLoader();
        $budgets = $budgets_loader->load($entityManager);
        $budget = current($budgets);
        // User
        $paul = $this->createUser('paul', $entityManager);
        // Add user to budgets
        $this->joinBudget($paul, $budget, $entityManager);
        // Add one contribution
        $contribution = new Contribution();
        $contribution->setBudget($budget);
        $contribution->setAmount(10);
        $contribution->setDescription('First contribution');
        $contribution->setDoned(new \DateTime());
        $contribution->setDuration(1);
        $contribution->setContributor($paul);
        $entityManager->persist($contribution);
        $entityManager->flush();

        /*
         * Delete as root
         */
        $this->logIn($client, 'root');
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/delete');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/budget'));
        // Get sessions
        $session = $client->getRequest()->getSession();
        $this->assertContains('success', $session->getFlashBag()->keys());
        $this->assertStringContainsString('deleted', current($session->getFlashBag()->get('success')));
    }

    public function testDeleteAsContributor(): void
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        // User
        $paul = $this->createUser('paul', $entityManager);

        /*
         * Ok : no contribution and no contributor
         */
        // Load new budget
        $budgets_loader = new BudgetLoader();
        $budgets = $budgets_loader->load($entityManager);
        /** @var Budget $budget */
        $budget = current($budgets);
        $this->logIn($client, 'paul');
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/delete');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/budget'));
        // Get session
        $session = $client->getRequest()->getSession();
        $this->assertContains('success', $session->getFlashBag()->keys());
        $this->assertStringContainsString('deleted', current($session->getFlashBag()->get('success')));
    }

    public function testDeleteAsContributorForbidden(): void
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        // User
        $paul = $this->createUser('paul', $entityManager);

        /*
         * Forbidden : already one contribution
         */
        // Load new budget
        $budgets_loader = new BudgetLoader();
        $budgets = $budgets_loader->load($entityManager);
        /** @var Budget $budget */
        $budget = current($budgets);
        // Add one contribution
        $contribution = new Contribution();
        $contribution->setBudget($budget);
        $contribution->setAmount(10);
        $contribution->setDescription('First contribution');
        $contribution->setDoned(new \DateTime());
        $contribution->setDuration(1);
        $contribution->setContributor($paul);
        $entityManager->persist($contribution);
        $entityManager->flush();
        // Login
        $this->logIn($client, 'paul');
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/delete');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isForbidden());

        /*
         * Forbidden : already on contributor
         */
        $pierre = $this->createUser('pierre', $entityManager);
        $budgets_loader = new BudgetLoader();
        $budgets = $budgets_loader->load($entityManager);
        /** @var Budget $budget */
        $budget = current($budgets);
        $this->joinBudget($pierre, $budget, $entityManager);
        $entityManager->flush();
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/delete');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isForbidden());
    }

    public function testHistory(): void
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        // Load budget
        $budgets_loader = new BudgetLoader();
        $budgets = $budgets_loader->load($entityManager);
        $budget = current($budgets);

        /*
         *  Check access
         */
        // Not logged in
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/history');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_FOUND, $client->getResponse()->getStatusCode(), 'Expect login redirection for /budget/*/history');
        $this->assertStringEndsWith('/login', $client->getResponse()->headers->get('Location'));
        // Not contributor
        $paul = $this->createUser('paul', $entityManager);
        $this->logIn($client, 'paul');
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/history');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        /*
         * No history
         */
        $raoul = $this->createUser('raoul', $entityManager);
        // Load budget
        $budgets_loader = new BudgetLoader();
        $budgets = $budgets_loader->load($entityManager);
        $budget = current($budgets);
        $this->joinBudget($raoul, $budget, $entityManager);
        $this->logIn($client, 'raoul');
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/history');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(0, $crawler->filter('.log-list-item'));

        /*
         * One contribution of other
         */
        // Add contribution
        $pierre = $this->createUser('pierre', $entityManager);
        $this->joinBudget($pierre, $budget, $entityManager);
        $contribution = new Contribution();
        $contribution->setBudget($budget);
        $contribution->setAmount(10);
        $contribution->setDescription('First contribution');
        $contribution->setDoned(new \DateTime());
        $contribution->setDuration(1);
        $contribution->setContributor($pierre);
        $entityManager->persist($contribution);
        $entityManager->flush();

        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/history');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('.log-list-item'));

        /*
         * Test filter on activity
         */
        // Add activity
        $activitySec = $this->createActivity('secretariat', $entityManager);
        $budget->addActivity($activitySec);
        $activityDev = $this->createActivity('developpement', $entityManager);
        $budget->addActivity($activityDev);
        // Add contribution
        $contribution = new Contribution();
        $contribution->setBudget($budget);
        $contribution->setAmount(20);
        $contribution->setDescription('First contribution');
        $contribution->setDoned(new \DateTime());
        $contribution->setDuration(2);
        $contribution->setContributor($pierre);
        $contribution->addActivity($activityDev);
        $entityManager->persist($contribution);
        $entityManager->flush();

        // no filter
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/history');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(2, $crawler->filter('.log-list-item'));

        // filter on one activity
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/history?activities[]=developpement');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('.log-list-item'));

        // filter on two activities
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/history?activities[]=developpement&activities[]=secretariat');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('.log-list-item'));

        // filter on one activity not used by a contribution
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/history?activities[]=secretariat');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(0, $crawler->filter('.log-list-item'));

        /*
         * Add contribution of logged user
         */
        // Add contribution
        $contribution = new Contribution();
        $contribution->setBudget($budget);
        $contribution->setAmount(null);
        $contribution->setDescription('First contribution');
        $contribution->setDoned(new \DateTime());
        $contribution->setContributor($raoul);
        $entityManager->persist($contribution);
        $entityManager->flush();

        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/history');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(3, $crawler->filter('.log-list-item'));
        $this->assertCount(1, $crawler->filter('.log-list-item.me'));
    }

    public function testDeleteMyContribution(): void
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        // Load budget
        $budgets_loader = new BudgetLoader();
        $budgets = $budgets_loader->load($entityManager);
        $budget = current($budgets);
        $paul = $this->createUser('paul', $entityManager);
        $pierre = $this->createUser('pierre', $entityManager);
        $this->joinBudget($paul, $budget, $entityManager);
        $this->joinBudget($pierre, $budget, $entityManager);
        // Add contribution paul
        $contribution = new Contribution();
        $contribution->setBudget($budget);
        $contribution->setAmount(10);
        $contribution->setDescription('First contribution');
        $contribution->setDoned(new \DateTime());
        $contribution->setDuration(1);
        $contribution->setContributor($paul);
        $entityManager->persist($contribution);
        $entityManager->flush();
        // Add contribution pierre
        $contribution = new Contribution();
        $contribution->setBudget($budget);
        $contribution->setDescription('Second contribution');
        $contribution->setDoned(new \DateTime());
        $contribution->setContributor($pierre);
        $entityManager->persist($contribution);
        $entityManager->flush();
        // Add contribution pierre (claimed)
        $contribution_claimed = new Contribution();
        $contribution_claimed->setBudget($budget);
        $contribution_claimed->setDescription('Third contribution');
        $contribution_claimed->setDoned(new \DateTime());
        $contribution_claimed->setContributor($pierre);
        $entityManager->persist($contribution_claimed);
        $retribution = $container->get(BudgetManager::class)->claim($pierre, [$contribution_claimed]);
        $entityManager->persist($retribution);
        $entityManager->flush();

        /*
         *  Check access : logged in and paul can't delete
         */
        // Anonymous
        $crawler = $client->request(Request::METHOD_GET, '/budget/log/'.$contribution_claimed->getId().'/delete');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_FOUND, $client->getResponse()->getStatusCode(), 'Expect login redirection for '.$client->getRequest()->getRequestUri());
        $this->assertStringEndsWith('/login', $client->getResponse()->headers->get('Location'));
        // Paul
        $this->logIn($client, 'paul');
        $crawler = $client->request(Request::METHOD_GET, '/budget/log/'.$contribution_claimed->getId().'/delete');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isForbidden());

        /*
         * Pierre could delete one contribution (owned and not claimed)
         */
        $this->logIn($client, 'pierre');
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/history');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('.log-list-item .btn-delete-log'));
        $delete_uri = $crawler->filter('.log-list-item .btn-delete-log')->first()->link();

        /*
         * Perform delete claimed
         */
        $this->logIn($client, 'pierre');
        $crawler = $client->request(Request::METHOD_GET, '/budget/log/'.$contribution_claimed->getId().'/delete');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/budget/'.$budget->getSlug().'/history'));
        $session = $client->getRequest()->getSession();
        $this->assertContains('error', $session->getFlashBag()->keys());
        $this->assertStringContainsString('action denied', current($session->getFlashBag()->get('error')));

        /*
         * Perform delete
         */
        $crawler = $client->click($delete_uri);
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/budget/'.$budget->getSlug().'/history'));
        $session = $client->getRequest()->getSession();
        $this->assertContains('success', $session->getFlashBag()->keys());
        $this->assertStringContainsString('deleted', current($session->getFlashBag()->get('success')));
    }

    public function testBudgetDetails(): void
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        // Load data
        $container->get('fidry_alice_data_fixtures.loader.doctrine')->load(
            [__DIR__.'/../../fixtures/tests/budgets/budget_detail_view.yaml']
        );

        // Log as user1
        $this->logIn($client, 'user1');

        // Search budget no retributions
        $budget = $doctrine->getRepository(Budget::class)->findOneBy(['title' => 'Test 2']);
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/detail');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        // Search budget with retributions
        $budget = $doctrine->getRepository(Budget::class)->findOneBy(['title' => 'Test 1']);
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/detail');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertCount(1, $crawler->filter('.retribution-list .retribution'));
    }

    public function testArchiveAuthorization(): void
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        // Load new budget
        $budgets_loader = new BudgetLoader();
        $budgets = $budgets_loader->load($entityManager);
        /** @var Budget $budget */
        $budget = current($budgets);

        // Deny if not logged
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/archive-close');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertEquals(Response::HTTP_FOUND, $client->getResponse()->getStatusCode(), 'Expect root denied for /budget/create');

        // Deny if not a contributor
        $user = $this->logIn($client);
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/archive-close');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isForbidden());
    }

    public function testArchiveByContributor(): void
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();

        // Load new budget
        $budgets_loader = new BudgetLoader();
        $budgets = $budgets_loader->load($entityManager);
        /** @var Budget $budget */
        $budget = current($budgets);

        // User close the budget
        $paul = $this->createUser('paul', $entityManager);
        $this->joinBudget($paul, $budget, $entityManager);
        $this->logIn($client, 'paul');

        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/archive-close');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/budget'));
        $session = $client->getRequest()->getSession();
        $this->assertContains('success', $session->getFlashBag()->keys());
        $this->assertStringContainsString('archived', current($session->getFlashBag()->get('success')));

        // User open the budget
        $crawler = $client->request(Request::METHOD_GET, '/budget/'.$budget->getSlug().'/archive-open');
        $this->assertFalse($client->getResponse()->isServerError(), $crawler->filter('title')->text());
        $this->assertTrue($client->getResponse()->isRedirect('/budget'));
        $session = $client->getRequest()->getSession();
        $this->assertContains('success', $session->getFlashBag()->keys());
        $messages = $session->getFlashBag()->get('success');
        end($messages);
        $this->assertStringContainsString('opened', current($messages));
    }
}

<?php

use App\Kernel;

require __DIR__.'/bootstrap.php';

$appKernel = new Kernel('dev', false);
$appKernel->boot();

return $appKernel->getContainer();

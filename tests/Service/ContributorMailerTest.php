<?php

namespace App\Tests\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\ContributorMailer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

final class ContributorMailerTest extends TestCase
{
    public function testSendMail(): void
    {
        $userRepository = $this->getMockBuilder(UserRepository::class)->disableOriginalConstructor()->getMock();
        $mailer = $this->getMockBuilder(MailerInterface::class)->disableOriginalConstructor()->getMock();
        $contMailer = new ContributorMailer($userRepository, $mailer);

        // Argument user invalid
        try {
            $contMailer->sendMail(null, '', '', '');
            $this->fail('Exception expected if bad $user argument');
        } catch (\InvalidArgumentException) {
            $this->assertTrue(true);
        }

        // Argument user invalid
        try {
            $contMailer->sendMail(1, '', '', '');
            $this->fail('Exception expected if use does not exist');
        } catch (\OutOfBoundsException) {
            $this->assertTrue(true);
        }

        // Mail send for an existing user (by id)
        $user = new User();
        $user->setEmail('pierre@dupont.com');
        $userRepository->method('find')->willReturn($user);
        $mailer->method('send')->with($this->callback(fn ($arg) => $arg instanceof Email && current($arg->getTo())->toString() == 'pierre@dupont.com'));
        $contMailer->sendMail(1, '', '', '');

        // Mail send for an existing user (by user)
        $userRepository = $this->getMockBuilder(UserRepository::class)->disableOriginalConstructor()->getMock();
        $mailer = $this->getMockBuilder(MailerInterface::class)->disableOriginalConstructor()->getMock();
        $contMailer = new ContributorMailer($userRepository, $mailer);
        $user = new User();
        $user->setEmail('anne@dupont.com');
        $mailer->method('send')->with($this->callback(fn ($arg) => $arg instanceof Email && current($arg->getTo())->toString() == 'anne@dupont.com'));
        $contMailer->sendMail($user, '', '', '');
    }
}

<?php

namespace App\Tests\Api;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use App\Test\Entity\UserTestTrait;

class ApiTest extends ApiTestCase
{
    use UserTestTrait;

    public function testEndpoints(): void
    {
        // Testing if endpoints are responding (html & json)
        $endpoints = ['activities', 'budgets', 'retributions', 'contributions', 'users'];

        foreach ($endpoints as $endpoint) {
            // Test for Html
            $response = static::createClient()->request('GET', '/api/'.$endpoint.'.html', [
                'headers' => ['Accept' => 'text/html'],
            ]);
            $this->assertResponseIsSuccessful();
            $this->assertResponseHeaderSame('Content-type', 'text/html; charset=UTF-8');

            // Test for Json
            $response = static::createClient()->request('GET', '/api/'.$endpoint.'.json', [
                'headers' => ['Accept' => 'application/json'],
            ]);

            $this->assertResponseIsSuccessful();
            $this->assertResponseHeaderSame('Content-type', 'application/json; charset=utf-8');
        }

        // For Users, if there is a user, test if password and salt are not exposed
        $users = $response->toArray();
        if (count($users) > 0) {
            $this->assertArrayNotHasKey('password', $response->toArray()[0]);
            $this->assertArrayNotHasKey('salt', $response->toArray()[0]);
        }
    }

    /**
     * Test that POST request is impossible for the API.
     */
    public function testDenyPostRequest(): void
    {
        $response = static::createClient()->request('POST', '/api/', [
            'headers' => ['Accept' => 'text/html'],
        ]);
        $this->assertResponseStatusCodeSame(404);
    }

    /**
     * Test default pagination settings :
     *
     *  - no pagination
     *  - pagination enabled with pagination parameter
     *
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function testPagination(): void
    {
        $client = static::createClient();

        // load 40 users
        for ($i = 1; $i <= 40; ++$i) {
            $this->createUser('alonzo'.$i, $client->getContainer()->get('doctrine')->getManager());
        }
        $response = $client->request('GET', '/api/users.json', [
            'headers' => ['Accept' => 'application/json'],
        ]);
        $users = json_decode($response->getContent());
        // No pagination
        $this->assertCount(40, $users);

        // With pagination parameter : limit to 30
        $response = $client->request('GET', '/api/users.json?pagination=true', [
            'headers' => ['Accept' => 'application/json'],
        ]);
        $users = json_decode($response->getContent());
        $this->assertCount(30, $users);
    }

    /**
     * No header exepected except if it's a request GET/OPTIONS on the api.
     */
    public function testControlOriginHeaders(): void
    {
        $response = static::createClient()->request('GET', '/api', [
            'headers' => ['Accept' => 'text/html'],
        ]);
        $this->assertResponseIsSuccessful();
        $headers = $response->getHeaders();
        $this->assertArrayHasKey('access-control-allow-origin', $response->getHeaders());
        $this->assertEquals('*', current($headers['access-control-allow-origin']), 'Expect all access-control-allow-origin for GET api request');

        // request out of api scope : no access-control-allow-origin expected
        $response = static::createClient()->request('GET', '/login', [
            'headers' => ['Accept' => 'text/html'],
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertResponseIsSuccessful();
        $this->assertArrayNotHasKey('access-control-allow-origin', $response->getHeaders());
    }

    /**
     * Test privacy for data exposed with API.
     *
     * Email and username : not readable
     *
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function testPrivacy(): void
    {
        $client = static::createClient();

        $this->createUser('alonzo', $client->getContainer()->get('doctrine')->getManager());
        $response = $client->request('GET', '/api/users.json', [
            'headers' => ['Accept' => 'application/json'],
        ]);
        $users = json_decode($response->getContent());
        $alonzo = current($users);
        // No pagination
        $this->assertObjectNotHasAttribute('email', $alonzo);
        $this->assertObjectNotHasAttribute('username', $alonzo);
    }
}

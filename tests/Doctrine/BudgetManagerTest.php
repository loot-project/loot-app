<?php

namespace App\Tests\Doctrine;

use App\Entity\Budget;
use App\Entity\Contribution;
use App\Entity\Manager\BudgetManager;
use App\Entity\Retribution;
use App\Test\Entity\BudgetTestTrait;
use App\Test\Entity\Loader\BudgetLoader;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\MailerAssertionsTrait;
use Symfony\Component\Mime\RawMessage;

class BudgetManagerTest extends KernelTestCase
{
    use BudgetTestTrait;
    use MailerAssertionsTrait;

    /**
     * @return RawMessage[]
     */
//    public static function getMailerMessages(?string $transport = null): array
//    {
//        if (!$logger = self::$container->get('mailer.logger_message_listener')) {
//            static::fail('A client must have Mailer enabled to make email assertions. Did you forget to require symfony/mailer?');
//        }
//
//        return $logger->getEvents()->getMessages($transport);
//    }

    public function testContributionTotalAmount(): void
    {
        $kernel = static::bootKernel();
        $budget_manager = $kernel->getContainer()->get(BudgetManager::class);

        $contribution1 = new Contribution();
        $contribution1->setAmount(10);
        $contribution2 = new Contribution();
        $contribution2->setAmount(20);

        $this->assertEquals(30, $budget_manager->contributionTotalAmount([$contribution1, $contribution2]));
    }

    public function testBudgetManagerClaimMethod(): void
    {
        $kernel = static::bootKernel();
        $budget_manager = $kernel->getContainer()->get(BudgetManager::class);
        $em = $kernel->getContainer()->get('doctrine.orm.default_entity_manager');

        // Load based data
        $budget_loader = new BudgetLoader();
        $budgets = $budget_loader->load($em);
        /** @var Budget $budget_tracked_with_amount */
        $budget_tracked_with_amount = current($budgets);
        $this->setReferent($this->addUser('referent', $em), $budget_tracked_with_amount, $em);

        $contributor = $this->addUser('contributor', $em);
        $contributions = $this->generateContributions($contributor, $budget_tracked_with_amount, $em);

        $retribution = $budget_manager->claim($contributor, $contributions);
        $date_time = new \DateTime();

        $this->assertInstanceOf(Retribution::class, $retribution);
        $this->assertEquals($retribution->getReference(), 'LT-'.$date_time->format('Y').'-000001');
        $this->assertCount(count($contributions), $retribution->getContributions());

        // Message sent
        $sentMail = $this->getMailerMessage(0);
        $this->assertEmailHeaderSame($sentMail, 'To', 'referent@localhost');
        $sentMail = $this->getMailerMessage(1);
        $this->assertEmailHeaderSame($sentMail, 'To', 'contributor@localhost');

        $this->assertStringContainsString($budget_tracked_with_amount->getTitle(), $sentMail->toString());
        $this->assertStringContainsString($retribution->getReference(), $sentMail->toString());
        $this->assertStringContainsString($budget_manager->contributionTotalAmount($contributions), $sentMail->toString());
    }
}

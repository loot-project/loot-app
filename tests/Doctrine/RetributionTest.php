<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 17/02/20
 * Time: 15:51.
 */

namespace App\Tests\Doctrine;

use App\Doctrine\RetributionReferenceGenerator;
use App\Entity\Retribution;
use App\Test\Entity\BudgetTestTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RetributionTest extends KernelTestCase
{
    use BudgetTestTrait;

    public function testReferenceGenerator(): void
    {
        $kernel = static::bootKernel();
        $em = $kernel->getContainer()->get('doctrine.orm.default_entity_manager');
        $retribution = new Retribution();

        // Generate the first retribution id
        $date_time = new \DateTime();

        $generator = new RetributionReferenceGenerator();
        $this->assertEquals('LT-'.$date_time->format('Y').'-000001', $generator->generate($em, $retribution));
    }
}

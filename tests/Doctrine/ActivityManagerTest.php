<?php

namespace App\Tests\Doctrine;

use App\Entity\Activity;
use App\Entity\Manager\ActivityManager;
use App\Test\Entity\Loader\BudgetLoader;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ActivityManagerTest extends KernelTestCase
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    public function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
                                      ->get('doctrine.orm.entity_manager');
        parent::setUp();
    }

    public function testRemoveUnusedActivity(): void
    {
        $manager = new ActivityManager(
            $this->entityManager, $this->entityManager->getRepository(Activity::class)
        );

        $activity = new Activity();
        $activity->setTitle('bidule');
        $this->entityManager->persist($activity);
        // Load budget
        $budget_loader = new BudgetLoader();
        $budgets = $budget_loader->load($this->entityManager);
        $budget = current($budgets);
        $budget->addActivity($activity);
        $activity->addBudget($budget);
        $this->entityManager->persist($budget);
        $activity = new Activity();
        $activity->setTitle('truc');
        $this->entityManager->persist($activity);
        $this->entityManager->flush();

        $this->assertCount(2, $this->entityManager->getRepository(Activity::class)->findAll());
        $this->assertCount(1, $this->entityManager->getRepository(Activity::class)->findActivitesUnused());
        $manager->removeUnusedActivity();
        $this->entityManager->flush();

        $this->assertCount(1, $this->entityManager->getRepository(Activity::class)->findAll());
    }
}

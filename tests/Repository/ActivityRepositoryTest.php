<?php

namespace App\Tests\Repository;

use App\Entity\Activity;
use App\Entity\Budget;
use App\Repository\ActivityRepository;
use App\Test\Entity\Loader\BudgetLoader;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ActivityRepositoryTest extends KernelTestCase
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    public function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
                           ->get('doctrine.orm.entity_manager');
        parent::setUp();
    }

    public function testFindActivitesUnused(): void
    {
        /** @var ActivityRepository $repository */
        $repository = $this->entityManager->getRepository(Activity::class);

        $activity = new Activity();
        $activity->setTitle('bidule');
        $this->entityManager->persist($activity);
        // Load budget
        $budget_loader = new BudgetLoader();
        $budgets = $budget_loader->load($this->entityManager);
        $budget = current($budgets);
        $budget->addActivity($activity);
        $activity->addBudget($budget);
        $this->entityManager->persist($budget);
        $this->entityManager->flush();

        $this->assertCount(1, $repository->findAll());
        $this->assertCount(0, $repository->findActivitesUnused());

        $activity = new Activity();
        $activity->setTitle('truc');
        $this->entityManager->persist($activity);
        $this->entityManager->flush();

        $this->assertCount(2, $repository->findAll());
        $this->assertCount(1, $repository->findActivitesUnused());
    }
}

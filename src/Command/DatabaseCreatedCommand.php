<?php

namespace App\Command;

use App\Entity\Budget;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:database-created',
    description: 'Add a short description for your command',
)]
class DatabaseCreatedCommand extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager
    )
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        // Test if the database is created with schema by testing if the table associated to the entity Budget exist
        // Get table of entity Budget
        $table = $this->entityManager->getClassMetadata(Budget::class)->getTableName();
        // Test if table exist
        $tableExist = $this->entityManager->getConnection()->createSchemaManager()->tablesExist([$table]);
        if ($tableExist)
            return Command::SUCCESS;

        $io->info('The database is not created with schema. Please run the command "php bin/console doctrine:schema:create" to create the database with schema.');
        return Command::FAILURE;
    }
}

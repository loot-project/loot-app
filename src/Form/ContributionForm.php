<?php

namespace App\Form;

use App\Entity\Activity;
use App\Entity\Budget;
use App\Entity\Contribution;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContributionForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Budget $budget */
        $budget = $options['budget'];

        /** @var Contribution $contribution */
        $contribution = $builder->getData();

        if (!is_null($budget->getUnitSymbol())) {
            $builder
                ->add('amount', IntegerType::class, [
                    'attr' => ['placeholder' => 'contribution.placeholder.amount'],
                    'label' => 'contribution.amount',
                ])
                ->add('rewarded', ChoiceType::class, [
                    'label' => 'contribution.rewarded',
                    'choices' => [
                        'Yes' => true,
                        'No' => false,
                    ],
                    'expanded' => true,
                    'required' => true,
                    'mapped' => false,
                    'empty_data' => true,
                    'data' => true,
                ])
            ;
        }

        $builder
            ->add('doned', DateType::class, [
                'label' => 'contribution.doned',
                'required' => true,
                'widget' => 'single_text',
                'empty_data' => 'now',
            ])
        ;

        // Ask duration if timetracking
        if ($budget->isTimetracking()) {
            $builder
                ->add('duration', NumberType::class, [
                    'attr' => ['placeholder' => 'contribution.placeholder.duration'],
                    'label' => 'contribution.duration',
                    'scale' => 2,
                    'required' => true,
                    'help' => 'contribution.help.duration',
                ])
            ;
        }

        $builder
            ->add('activities', EntityType::class, [
                'class' => Activity::class,
                'attr' => [
                    'placeholder' => 'budget.placeholder.activities',
                    'class' => 'select-activities',
                ],
                'label' => 'budget.activities',
                'choice_label' => 'title',
                'choices' => $budget->getActivities(),
                'multiple' => true,
                'required' => false,
                'by_reference' => false,
            ])
            ->add('description', TextareaType::class, [
                'attr' => ['placeholder' => 'contribution.placeholder.description'],
                'label' => 'contribution.description',
                'required' => true,
                'help' => 'contribution.help.description',
            ])
            ->add('save', SubmitType::class, [
                'label' => 'contribution.add',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contribution::class,
            'translation_domain' => 'loot',
        ]);

        $resolver->setRequired('budget');

        $resolver->setAllowedTypes('budget', Budget::class);
    }
}

<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class ChangePasswordFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'options' => [
                    'attr' => [
                        'autocomplete' => 'new-password',
                    ],
                ],
                'first_options' => [
                    'constraints' => [
                        new NotBlank([
                            'message' => 'password.mail_needed',
                        ]),
                        new Regex([
                            'pattern' => '/(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{2,}/',
                            'match' => true,
                            'message' => 'password.one_letter_one_number',
                        ]),
                        new Regex([
                            'pattern' => '/[^a-zA-Z0-9]+/',
                            'match' => true,
                            'message' => 'password.one_special_letter',
                        ]),
                        new Length([
                            'min' => 6,
                            'minMessage' => 'password.min_characters',
                            // max length allowed by Symfony for security reasons
                            'max' => 4096,
                        ]),
                    ],
                    'label' => 'my_account.new_password',
                ],
                'second_options' => [
                    'label' => 'my_account.repeat_password',
                ],
                'invalid_message' => 'password.password_match',
                // Instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['translation_domain' => 'loot']);
    }
}

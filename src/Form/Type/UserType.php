<?php

namespace App\Form\Type;

use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'class' => User::class,
            'choice_label' => 'name',
            'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('u')
                ->orderBy('u.name', 'ASC'),
        ]);
    }

    public function getParent(): string
    {
        return EntityType::class;
    }
}

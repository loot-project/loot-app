<?php

namespace App\Form;

use App\Entity\Activity;
use App\Entity\Budget;
use App\Form\Type\UserType;
use App\Repository\ActivityRepository;
use App\Repository\ContributionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class BudgetForm extends AbstractType
{
    public function __construct(
        private readonly Security $security,
        private readonly ActivityRepository $activityRepository,
        private readonly ContributionRepository $contributionRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Budget $budget */
        $budget = $builder->getData();

        /** @var bool $isNew */
        $isNew = is_null($budget->getId());

        /** @var bool $hasContributions */
        $hasContributions = !$isNew && $this->contributionRepository
            ->countContributions($budget);

        $builder
            ->add('title', TextType::class, [
                'attr' => ['placeholder' => 'budget.placeholder.title'],
                'label' => 'budget.title',
            ])
            ->add('subtitle', TextType::class, [
                'attr' => ['placeholder' => 'budget.placeholder.subtitle'],
                'label' => 'budget.subtitle',
                'required' => false,
            ])
            ->add('activities', EntityType::class, [
                'class' => Activity::class,
                'query_builder' => fn (ActivityRepository $ar) => $ar->createQueryBuilder('a')
                    ->orderBy('a.title', 'ASC'),
                'choice_label' => 'title',
                'multiple' => true,
                'by_reference' => false,
                'attr' => [
                    'placeholder' => 'budget.placeholder.activities',
                    'class' => 'select-activities',
                    'data-allow-add' => true,
                ],
                'label' => 'budget.activities',
            ])
            ->add('rules', UrlType::class, [
                'attr' => ['placeholder' => 'budget.placeholder.rules'],
                'label' => 'budget.rules',
                'required' => false,
            ])
            ->add('referent', UserType::class, [
                'attr' => ['placeholder' => 'budget.placeholder.referent'],
                'label' => 'budget.referent',
                'data' => $this->security->getUser(),
                'required' => true,
            ])
            ->add('tags', TextType::class, [
                'attr' => ['placeholder' => 'budget.placeholder.tags'],
                'label' => 'budget.tags',
                'help' => 'budget.help.tags_lowercase',
                'required' => false,
            ])
            ->add('amount', IntegerType::class, [
                'attr' => ['placeholder' => 'budget.placeholder.amount'],
                'label' => 'budget.amount',
                'required' => false,
                'help' => $hasContributions
                    ? sprintf('%s (%s)', $budget->getUnit(), $budget->getUnitSymbol())
                    : null,
            ])
        ;

        // If activities field has data with values beginning with `_`, it means we have to create them before submitting the form. Otherwier the activity will be invalid.
        $em = $this->entityManager;
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($em) {
            $data = $event->getData();
            foreach ($data['activities'] ?? [] as $k => $v) {
                if (str_starts_with($v, '_')) {
                    $title = substr($v, 1);
                    $activity = $this->activityRepository->findOneBy(['title' => $title]);
                    if (is_null($activity)) {
                        $activity = new Activity();
                        $activity->setTitle($title);
                        $em->persist($activity);
                        $em->flush();

                        // Replace the value
                        $data['activities'][$k] = $activity->getId();
                    }
                }
            }
            $event->setData($data);
        });

        if ($isNew || !$hasContributions) {
            $builder
                ->add('unit', TextType::class, [
                    'attr' => ['placeholder' => 'budget.placeholder.unit'],
                    'label' => 'budget.unit',
                    'required' => false,
                ])
                ->add('unitSymbol', TextType::class, [
                    'attr' => ['placeholder' => 'budget.placeholder.unit_symbol'],
                    'label' => 'budget.unit_symbol',
                    'required' => false,
                ])
            ;
        }

        $builder
            ->add('timetracking', CheckboxType::class, [
                'label' => 'budget.timetracking',
                'required' => false,
            ])
            ->add('save', SubmitType::class, [
                'label' => $isNew ? 'budget.button.add' : 'budget.button.edit',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Budget::class,
            'translation_domain' => 'loot',
        ]);
    }
}

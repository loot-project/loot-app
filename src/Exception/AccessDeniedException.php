<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 17/02/20
 * Time: 15:00.
 */

namespace App\Exception;

class AccessDeniedException extends \Exception
{
    public function __construct(string $message = 'Access denied', int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct(
            $message, $code, $previous
        );
    }
}

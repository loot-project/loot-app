<?php

namespace App\Controller;

use App\Entity\Retribution;
use App\Repository\RetributionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class RetributionController extends AbstractController
{
    #[Route(path: '/retribution', name: 'app_retribution')]
    public function index(RetributionRepository $retributionRepository): \Symfony\Component\HttpFoundation\Response
    {
        $retributions = $retributionRepository->findBy([], ['reference' => 'desc']);
        $retribution_total = [];
        /** @var Retribution $retribution */
        foreach ($retributions as $retribution) {
            $retribution->totalAmount = 0;
            foreach ($retribution->getContributions() as $contribution) {
                $retribution->totalAmount += $contribution->getAmount();
            }
        }

        return $this->render('retribution/index.html.twig', [
            'retributions' => $retributions,
        ]);
    }
}

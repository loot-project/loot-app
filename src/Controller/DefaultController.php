<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 07/01/20
 * Time: 15:50.
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route(path: '/', name: 'index')]
    public function home(): \Symfony\Component\HttpFoundation\RedirectResponse
    {
        if ($this->getUser()) {
            return $this->redirect(
                $this->generateUrl('app_dashboard')
            );
        }

        return $this->redirect($this->generateUrl('app_login'));
    }

    #[Route(path: '/app/dashboard', name: 'app_dashboard')]
    public function dashboard(): \Symfony\Component\HttpFoundation\Response
    {
        return $this->render('welcome.html.twig');
    }
}

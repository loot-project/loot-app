<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\BudgetRepository;
use App\Repository\RetributionRepository;
use App\Repository\UserRepository;
use App\Service\ContributorMailer;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Contracts\Translation\TranslatorInterface;

class AccountController extends AbstractController
{
    public function __construct(private readonly \Doctrine\Persistence\ManagerRegistry $managerRegistry)
    {
    }

    #[Route(path: '/account', name: 'app_account')]
    public function index(UserRepository $userRepository): Response
    {
        // Get users
        $users = $userRepository->findBy([], ['name' => 'asc']);

        return $this->render('account/index.html.twig', [
            'users' => $users, 'user' => $this->getUser(),
        ]);
    }

    #[Route(path: '/account/create', name: 'app_account_create')]
    #[Route(path: '/account/{user_id}/edit', name: 'app_account_edit')]
    #[Route(path: '/account/{user_id}/edit-contributor', name: 'app_account_edit_from_profile', defaults: ['from' => 'profile'])]
    #[Route(path: '/account/{user_id}/edit-me', name: 'app_account_edit_me', defaults: ['from' => 'myaccount'])]
    public function create(Request $request,
        UserPasswordHasherInterface $passwordHasher,
        TranslatorInterface $translator,
        AuthorizationCheckerInterface $securityChecker,
        ContributorMailer $mailer,
        UserRepository $userRepository,
        $user_id = null, $from = null)
    {
        if ($user_id > 0) {
            $user = $userRepository->find($user_id);
            if (is_null($user)) {
                throw $this->createNotFoundException(sprintf('User id %d not found', $user_id));
            }

            $this->denyAccessUnlessGranted('edit', $user);
        } else {
            $user = new User();
        }

        $formBuilder = $this->createFormBuilder($user, ['data_class' => User::class, 'translation_domain' => 'loot']);
        $formBuilder->add(
            'firstname', TextType::class, [
                'attr' => ['placeholder' => 'account.placeholder.firstname'],
                'label' => 'account.firstname',
            ]
        )->add(
            'lastname', TextType::class, [
                'attr' => ['placeholder' => 'account.placeholder.lastname'],
                'label' => 'account.lastname',
            ]
        );
        // Email for create or edit by root
        if ($user->getId() <= 0 || $securityChecker->isGranted('ROLE_SUPER_ADMIN')) {
            $formBuilder->add(
                'email', EmailType::class, [
                    'attr' => ['placeholder' => 'account.placeholder.email'],
                    'label' => 'account.email',
                ]
            );
        }
        // generate new password if edit by root
        if ($user_id > 0 && $securityChecker->isGranted('ROLE_SUPER_ADMIN')) {
            $formBuilder->add(
                'new_password', CheckboxType::class, [
                    'label' => 'account.edit_view.generate_password', 'mapped' => false,
                ]
            );
        }
        // Checkbox to confirm the sending of the password by mail
        $mail_message_example = null;
        if ($user->getId() <= 0) {
            $formBuilder->add(
                'send_passsword', CheckboxType::class, [
                    'label' => 'account.send_password_mail', 'mapped' => false, 'data' => true,
                ]
            );
            $formBuilder->add(
                'custom_message', TextareaType::class, [
                    'attr' => ['placeholder' => 'account.placeholder.custom_message'],
                    'label' => 'account.custom_message', 'mapped' => false,
                    'block_prefix' => ' custom_message_group',
                ]
            );
            $mail_message_example = $this->renderView(
                'account/email_send_password.html.twig', [
                    'email' => 'mail@example.com',
                    'password' => 'p_ssword',
                    'custom_message' => '<i>'.$translator->trans('account.custom_message', [], 'loot').'</i>',
                    'email_creator' => ($securityChecker->isGranted('ROLE_SUPER_ADMIN')) ? $_ENV['MAILER_SENDER'] : $this->getUser()->getEmail(),
                ]);
        }
        // Button type
        if ($user->getId() > 0) {
            $formBuilder->add('save', SubmitType::class, ['label' => 'account.edit_view.edit_submit_button']);
        } else {
            $formBuilder->add('save', SubmitType::class, ['label' => 'account.add_user']);
        }

        $form = $formBuilder->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // Generate password if required
            $plain_password = null;
            if ($user_id > 0 && $form->has('new_password') && $form->get('new_password')->getData()) {
                $plain_password = substr(
                    str_shuffle('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz'), 0, 8
                );
            } elseif (is_null($user_id)) {
                $plain_password = substr(
                    str_shuffle('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz'), 0, 8
                );
            }
            // Encode password
            if (!is_null($plain_password)) {
                // 3) Encode the password (you could also do this via Doctrine listener)
                $password = $passwordHasher->hashPassword($user, $plain_password);
                $user->setPassword($password);
            }
            // Generate display name
            if (!is_null($user->getFirstname()) && !is_null($user->getLastname())) {
                $user->setName(
                    $translator->trans('account.name_format', ['%firstname%' => ucfirst($user->getFirstname()), '%lastname%' => strtoupper($user->getLastname())], 'loot')
                );
            } else {
                $user->setName($user->getEmail());
            }
            // Save user
            $em = $this->managerRegistry->getManager();
            if (is_null($user_id)) {
                $em->persist($user);
            }

            try {
                $em->flush();
                $message = $translator->trans('account.edit_success', ['%email%' => $user->getEmail()], 'loot');
                if (!empty($plain_password)) {
                    if ($form->has('send_passsword') && $form->get('send_passsword')->getData()) {
                        if ($securityChecker->isGranted('ROLE_SUPER_ADMIN')) {
                            $email_creator = $_ENV['MAILER_SENDER'];
                        } else {
                            $email_creator = $this->getUser()->getEmail();
                        }
                        $vars = ['email' => $user->getEmail(),
                            'password' => $plain_password,
                            'custom_message' => $form->get('custom_message')->getData(),
                            'email_creator' => $email_creator,
                        ];
                        $subject = $translator->trans('account.welcome_mail.subject', [], 'loot');
                        $mail_html = $this->renderView('account/email_send_password.html.twig', $vars);
                        $mail_txt = $this->renderView('account/email_send_password.txt.twig', $vars);
                        $mailer->sendMail($user, $subject, $mail_txt, $mail_html);
                    } else {
                        $subject = rawurlencode($translator->trans('account.send_password_subject',
                            ['%password%' => $plain_password], 'loot'));
                        $message .= ' '.$translator->trans('account.send_password', [
                            '%password%' => $plain_password,
                            '%email%' => $user->getEmail(),
                            '%subject%' => $subject,
                        ], 'loot');
                    }
                }
                $this->addFlash('success', $message);
                if ($from == 'myaccount') {
                    return $this->redirectToRoute('app_my_account');
                }

                return $this->redirectToRoute('app_account');
            } catch (UniqueConstraintViolationException) {
                $this->addFlash(
                    'danger',
                    $translator->trans('account.already_exist', ['%email%' => $user->getEmail()], 'loot')
                );
            }
        }

        $template = 'account/create.html.twig';
        if ($user_id > 0) {
            $template = 'account/edit.html.twig';
        }

        // Back button
        $backlink = match ($from) {
            'profile' => $this->generateUrl('app_account_view', ['user_id' => $user_id]),
            'myaccount' => $this->generateUrl('app_my_account'),
            default => $this->generateUrl('app_account'),
        };

        return $this->render($template, [
            'form' => $form->createView(), 'user_id' => $user_id, 'backlink' => $backlink,
            'mail_message_example' => $mail_message_example,
        ]);
    }

    #[Route(path: '/account/me/', name: 'app_my_account')]
    public function myaccount(): Response
    {
        return $this->render('account/me/index.html.twig');
    }

    /**
     * @return Response
     */
    #[Route(path: '/account/me/edit-mail', name: 'app_my_account_edit_mail')]
    public function changeEmail(Request $request, TranslatorInterface $translator)
    {
        /** @var User $user */
        $user = $this->getUser();

        $this->denyAccessUnlessGranted('edit', $user);

        $formBuilder = $this->createFormBuilder(['email' => $user->getEmail()], ['translation_domain' => 'loot']);
        $formBuilder->add(
            'email', EmailType::class, [
                'attr' => ['placeholder' => 'Email'], 'constraints' => [new NotBlank(), new Email()],
            ]
        )->add('save', SubmitType::class, ['label' => 'my_account.button_save_email']);

        $form = $formBuilder->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $form_data = $form->getData();
            $user->setEmail($form_data['email']);

            try {
                $this->managerRegistry->getManager()->flush();
                $this->addFlash(
                    'success',
                    $translator->trans('my_account.edit_mail_success', [], 'loot')
                );

                return $this->redirectToRoute('app_my_account_edit_mail');
            } catch (UniqueConstraintViolationException) {
                $this->addFlash(
                    'danger',
                    $translator->trans('my_account.mail_already_exist', ['%email%' => $user->getEmail()], 'loot')
                );
            }
        }

        return $this->render('account/me/edit_mail.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @return Response
     */
    #[Route(path: '/account/me/edit-password', name: 'app_my_account_edit_password')]
    public function changePassword(Request $request, TranslatorInterface $translator, UserPasswordHasherInterface $passwordHasher)
    {
        /** @var User $user */
        $user = $this->getUser();

        $this->denyAccessUnlessGranted('edit', $user);

        $formBuilder = $this->createFormBuilder([], ['translation_domain' => 'loot']);
        $formBuilder->add(
            'old_password', TextType::class, [
                'attr' => ['placeholder' => ''], 'constraints' => [
                    new NotBlank(),
                ],
                'label' => 'my_account.current_password',
            ]
        )->add(
            'new_password', TextType::class, [
                'attr' => ['placeholder' => ''], 'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 8, 'minMessage' => 'password.min_characters']),
                    new Regex([
                        'pattern' => '/(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{2,}/',
                        'match' => true,
                        'message' => 'password.one_letter_one_number',
                    ]),
                    new Regex([
                        'pattern' => '/[^a-zA-Z0-9]+/',
                        'match' => true,
                        'message' => 'password.one_special_letter',
                    ]),
                ], 'label' => 'my_account.new_password',
                'help' => 'my_account.password_helper',
            ]
        )->add('save', SubmitType::class, ['label' => 'my_account.button_new_password']);

        $form = $formBuilder->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $passwords = $form->getData();
            if ($passwordHasher->isPasswordValid($user, $passwords['old_password'])) {
                $user->setPassword($passwordHasher->hashPassword($user, $passwords['new_password']));
                $this->managerRegistry->getManager()->flush();
                $this->addFlash(
                    'success',
                    $translator->trans('my_account.edit_password_success', [], 'loot')
                );

                return $this->redirectToRoute('app_my_account');
            } else {
                $form->get('old_password')->addError(new FormError($translator->trans('my_account.bad_password', [], 'loot')));
            }
        }

        return $this->render('account/me/edit_password.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Display view of a contributor.
     */
    #[Route(path: '/account/{user_id}/view', name: 'app_account_view')]
    #[Route(path: '/account/{user_id}/delete-view', name: 'app_account_delete_view', defaults: ['delete_access' => true])]
    public function contributor(
        UserRepository $userRepository,
        BudgetRepository $budgetRepository,
        RetributionRepository $retributionRepository,
        $user_id,
        $delete_access = false
    ): Response {
        $user = $userRepository->find($user_id);
        if (is_null($user)) {
            throw $this->createNotFoundException(sprintf('User id %d not found', $user_id));
        }

        $this->denyAccessUnlessGranted('view', $user);

        $budgets = $budgetRepository->findByUser($user);

        $retributions = $retributionRepository->findByUser($user);
        foreach ($retributions as $retribution) {
            $retribution->totalAmount = 0;
            foreach ($retribution->getContributions() as $contribution) {
                $retribution->totalAmount += $contribution->getAmount();
            }
        }

        return $this->render('account/contributor.html.twig', ['contributor' => $user, 'budgets' => $budgets, 'retributions' => $retributions, 'delete_access' => $delete_access]);
    }

    /**
     * Delete a contributor.
     *
     * @return Response
     */
    #[Route(path: '/account/{user_id}/delete', name: 'app_account_delete')]
    public function delete(
        $user_id,
        TranslatorInterface $translator,
        Session $session,
        TokenStorageInterface $storage,
        UserRepository $userRepository,
        BudgetRepository $budgetRepository
    ): \Symfony\Component\HttpFoundation\RedirectResponse {
        $user = $userRepository->find($user_id);
        if (is_null($user)) {
            throw $this->createNotFoundException(sprintf('User id %d not found', $user_id));
        }

        $this->denyAccessUnlessGranted('delete', $user);
        $is_delete_logged_user = ($this->getUser() instanceof User) && $user_id == $this->getUser()->getId();
        $username = $user->getUserIdentifier();

        $budgets = $budgetRepository->findByUser($user);
        if (count($budgets) > 0) {
            $this->addFlash(
                'error',
                $translator->trans('account.delete.budgets_leave_required', [], 'loot')
            );

            return $this->redirectToRoute('app_account_view', ['user_id' => $user_id]);
        } else {
            $this->managerRegistry->getManager()->remove($user);
            $this->managerRegistry->getManager()->flush();
            if ($is_delete_logged_user) {
                $session->invalidate();
                $storage->setToken(null);
            }
            $this->addFlash(
                'success',
                $translator->trans('account.delete.success', ['%username%' => $username], 'loot')
            );
            if ($is_delete_logged_user) {
                return $this->redirectToRoute('app_login');
            }

            return $this->redirectToRoute('app_account');
        }
    }
}

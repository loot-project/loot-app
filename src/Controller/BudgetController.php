<?php

namespace App\Controller;

use App\Entity\Budget;
use App\Entity\Contribution;
use App\Entity\Manager\ActivityManager;
use App\Entity\Manager\BudgetManager;
use App\Entity\Retribution;
use App\Entity\User;
use App\Form\BudgetForm;
use App\Form\ContributionForm;
use App\Repository\BudgetRepository;
use App\Repository\ContributionRepository;
use App\Repository\RetributionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class BudgetController extends AbstractController
{
    #[Route(path: '/budget', name: 'app_budget')]
    public function index(BudgetRepository $budgetRepository): Response
    {
        $budgets = $budgetRepository->findBy([], ['title' => 'asc']);

        return $this->render('budget/index.html.twig', [
            'budgets' => $budgets,
        ]);
    }

    #[Route(path: '/budget/create', name: 'app_budget_create')]
    #[Route(path: '/budget/{slug}/edit', name: 'app_budget_edit')]
    public function edit(Request $request,
        TranslatorInterface $translator, ActivityManager $activityManager,
        BudgetRepository $budgetRepository, EntityManagerInterface $em, BudgetManager $budgetManager,
        $slug = null): Response
    {
        $budget = new Budget();

        if (!is_null($slug)) {
            /** @var Budget $budget */
            $budget = $budgetRepository->findOneBy(['slug' => $slug]);
            if (is_null($budget)) {
                throw $this->createNotFoundException(sprintf('Budget %s not found', $slug));
            }
            $this->denyAccessUnlessGranted('edit', $budget);
        } else {
            $this->denyAccessUnlessGranted('create', $budget);
        }

        $form = $this->createForm(BudgetForm::class, $budget);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->getConnection()->beginTransaction();
            try {
                if (is_null($slug)) {
                    $uuidGenerator = new UuidGenerator();
                    $budget->setSlug($uuidGenerator->generate($em, $budget));
                    $em->persist($budget);

                    // Add the creator as contributor
                    $budgetManager->join($this->getUser(), $budget);
                }
                $em->flush();

                // To avoid activities list for next budget edition
                $activityManager->removeUnusedActivity(
                    $budget->getActivities()->toArray()
                );
                $em->flush();
                $em->getConnection()->commit();
            } catch (\Exception $e) {
                $em->getConnection()->rollBack();
                throw $e;
            }

            $this->addFlash(
                'success',
                $translator->trans('budget.success_form.'.((is_null($slug)) ? 'created' : 'edited'), [], 'loot')
            );

            return $this->redirectToRoute('app_budget');
        }

        return $this->render('budget/edit.html.twig', [
            'form' => $form->createView(), 'budget' => $budget, 'slug' => $slug,
        ]);
    }

    #[Route(path: '/budget/{slug}/join', name: 'app_budget_join')]
    public function join($slug,
        TranslatorInterface $translator,
        BudgetRepository $budgetRepository,
        EntityManagerInterface $em,
        BudgetManager $budgetManager
    ): Response {
        /** @var Budget $budget */
        $budget = $budgetRepository->findOneBy(['slug' => $slug]);
        if (is_null($budget)) {
            throw $this->createNotFoundException(sprintf('Budget %s not found', $slug));
        }

        $this->denyAccessUnlessGranted('join', $budget);

        $join_status = $budgetManager->join($this->getUser(), $budget);
        if($join_status === true) {
            $em->flush();
            $this->addFlash(
                'success',
                $translator->trans('budget.success_joining', ['%log_url%' => $this->generateUrl('app_budget_log', ['slug' => $slug])], 'loot')
            );

            return $this->redirectToRoute('app_budget');
        } else {
            $this->addFlash(
                'info',
                $translator->trans('budget.already_joined', [], 'loot')
            );
            return $this->redirectToRoute('app_budget');
        }
    }

    #[Route(path: '/budget/{slug}/leave', name: 'app_budget_leave')]
    public function leave($slug,
        TranslatorInterface $translator,
        ContributionRepository $contributionRepository,
        BudgetRepository $budgetRepository,
        EntityManagerInterface $em
    ): Response {
        /** @var Budget $budget */
        $budget = $budgetRepository->findOneBy(['slug' => $slug]);
        if (is_null($budget)) {
            throw $this->createNotFoundException(sprintf('Budget %s not found', $slug));
        }

        // Check if already joined
        /** @var User $user */
        $user = $this->getUser();
        if ($budget->getContributors()->contains($user)) {
            if ($contributionRepository->isUserHasContributionsForBudgetToClaim($user, $budget)) {
                $this->addFlash(
                    'danger',
                    $translator->trans('budget.denied_leaving', ['%budget%' => $budget->getTitle()], 'loot')
                );
            } else {
                $budget->getContributors()->removeElement($user);
                $user->getBudgets()->removeElement($budget);
                $em->flush();
                $this->addFlash(
                    'info', $translator->trans('budget.success_leaving', ['%budget%' => $budget->getTitle()], 'loot')
                );
            }
        }

        return $this->redirectToRoute('app_budget');
    }

    #[Route(path: '/budget/{slug}/log', name: 'app_budget_log')]
    public function log(Request $request, $slug,
        TranslatorInterface $translator,
        BudgetRepository $budgetRepository,
        EntityManagerInterface $em
    ): Response {
        $contribution = new Contribution();

        /** @var Budget $budget */
        $budget = $budgetRepository->findOneBy(['slug' => $slug]);
        if (is_null($budget)) {
            throw $this->createNotFoundException(sprintf('Budget %s not found', $slug));
        }

        $this->denyAccessUnlessGranted('contribute', $budget);

        $form = $this->createForm(ContributionForm::class, $contribution, ['budget' => $budget]);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            /** @var Contribution $contribution */
            $contribution = $form->getData();

            if (!is_null($budget->getUnitSymbol())) {
                $rewarded = $form->get('rewarded')->getData();

                // Amount required ?
                if ($rewarded && is_null($contribution->getAmount())) {
                    $form->get('amount')->addError(
                        new FormError($translator->trans('contribution.amount_required', [], 'validators'))
                    );
                } elseif (!$rewarded) {
                    $contribution->setAmount(null);
                }
            }

            if ($form->isValid()) {
                $contribution->setContributor($this->getUser());
                $contribution->setBudget($budget);

                $em->persist($contribution);
                $em->flush();

                $this->addFlash(
                    'success', $translator->trans('contribution.creation_success_and_continue', [], 'loot')
                );

                return $this->redirectToRoute('app_budget_log', ['slug' => $budget->getSlug()]);
            }
        }

        return $this->render('budget/log.html.twig', [
            'budget' => $budget, 'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/budget/{slug}/claim', name: 'app_budget_claim')]
    public function claim(Request $request, $slug,
        TranslatorInterface $translator,
        BudgetManager $budgetManager,
        ContributionRepository $contributionRepository,
        BudgetRepository $budgetRepository,
        EntityManagerInterface $em
    ): Response {
        $activities_filter = $request->query->get('activities');

        /** @var Budget $budget */
        $budget = $budgetRepository->findOneBy(['slug' => $slug]);
        if (is_null($budget)) {
            throw $this->createNotFoundException(sprintf('Budget %s not found', $slug));
        }

        $this->denyAccessUnlessGranted('contribute', $budget);

        /* list contribution not claimed */
        $contributions = $contributionRepository->getContributionsToClaim(
            $this->getUser(), $budget
        );

        if ($activities_filter && count($activities_filter) > 0) {
            $contributions = $this->filterByActivities($contributions, $activities_filter);
        }

        if ($request->getMethod() == 'POST') {
            $claimed_id = $request->get('claim-ids', '');
            // Search contribution
            $claimed_id = explode(',', (string) $claimed_id);
            $contributions_claimed = $contributionRepository->findBy(['id' => $claimed_id]);
            $retribution = $budgetManager->claim($this->getUser(), $contributions_claimed);
            if (is_null($retribution)) {
                return $this->redirectToRoute('app_budget_claim', ['slug' => $budget->getSlug()]);
            } else {
                $em->persist($retribution);
                $em->flush();
                $this->addFlash(
                    'success',
                    $translator->trans('claim.creation_success', [], 'loot')
                );

                return $this->redirectToRoute('app_budget_claim_view', [
                    'slug' => $budget->getSlug(),
                    'id' => $retribution->getId(),
                ]);
            }
        }

        return $this->render('budget/claim.html.twig', [
            'budget' => $budget,
            'contributions' => $contributions,
            'activities_filter' => $activities_filter,
        ]);
    }

    #[Route(path: '/budget/{slug}/{id}/claim-view', name: 'app_budget_claim_view')]
    public function claimView($slug, $id,
        BudgetRepository $budgetRepository,
        RetributionRepository $retributionRepository
    ): Response {
        /** @var Budget $budget */
        $budget = $budgetRepository->findOneBy(['slug' => $slug]);
        if (is_null($budget)) {
            throw $this->createNotFoundException(sprintf('Budget %s not found', $slug));
        }

        /** @var Retribution $retribution */
        $retribution = $retributionRepository->find($id);
        if (is_null($retribution)) {
            throw $this->createNotFoundException(sprintf('Retribution %d not found', $id));
        }

        $this->denyAccessUnlessGranted('view', $retribution);

        return $this->render('budget/claim_view.html.twig', [
            'budget' => $budget, 'retribution' => $retribution,
        ]);
    }

    #[Route(path: '/budget/{slug}/detail', name: 'app_budget_detail')]
    #[Route(path: '/budget/{slug}/delete-view', name: 'app_budget_delete_view', defaults: ['from' => 'delete'])]
    public function detail($slug,
        BudgetManager $budgetManager,
        BudgetRepository $budgetRepository,
        RetributionRepository $retributionRepository,
        $from = null): Response
    {
        /** @var Budget $budget */
        $budget = $budgetRepository->findOneBy(['slug' => $slug]);
        if (is_null($budget)) {
            throw $this->createNotFoundException(sprintf('Budget %s not found', $slug));
        }

        $retributionsWithTotalAmount =
            array_map(
                function ($r) use ($budgetManager) {
                    $r->totalAmount = $budgetManager->contributionTotalAmount($r->getContributions());

                    return $r;
                },
                $retributionRepository->findByBudget($budget)
            );

        return $this->render('budget/detail.html.twig', [
            'budget' => $budget, 'from' => $from, 'retributions' => $retributionsWithTotalAmount,
        ]);
    }

    #[Route(path: '/budget/{slug}/history', name: 'app_budget_history')]
    public function history($slug, ContributionRepository $contributionRepository, Request $request,
        BudgetRepository $budgetRepository
    ): Response {
        $activities_filter = $request->query->all()['activities'] ?? [];
        /** @var Budget $budget */
        $budget = $budgetRepository->findOneBy(['slug' => $slug]);
        if (is_null($budget)) {
            throw $this->createNotFoundException(sprintf('Budget %s not found', $slug));
        }

        $contributions = $contributionRepository->findBy(['budget' => $budget], ['doned' => 'DESC']);

        if ($activities_filter && count($activities_filter) > 0) {
            $contributions = $this->filterByActivities($contributions, $activities_filter);
        }

        return $this->render('budget/history.html.twig', [
            'budget' => $budget,
            'contributions' => $contributions,
            'activities_filter' => $activities_filter,
        ]);
    }

    /**
     * Delete a budget.
     */
    #[Route(path: '/budget/{slug}/delete', name: 'app_budget_delete')]
    public function delete($slug, TranslatorInterface $translator,
        BudgetRepository $budgetRepository,
        EntityManagerInterface $em
    ): Response {
        /** @var Budget $budget */
        $budget = $budgetRepository->findOneBy(['slug' => $slug]);
        if (is_null($budget)) {
            throw $this->createNotFoundException(sprintf('Budget %s not found', $slug));
        }

        $this->denyAccessUnlessGranted('delete', $budget);

        $em->remove($budget);
        $em->flush();
        $this->addFlash(
            'success',
            $translator->trans('budget.delete.success', ['%budget%' => $budget->getTitle()], 'loot')
        );

        return $this->redirectToRoute('app_budget');
    }

    /**
     * Archive a budget.
     */
    #[Route(path: '/budget/{slug}/archive-{direction}', name: 'app_budget_archive', defaults: ['direction' => 'close'])]
    public function archive($slug, TranslatorInterface $translator, $direction,
        BudgetRepository $budgetRepository,
        EntityManagerInterface $em
    ): Response {
        /** @var Budget $budget */
        $budget = $budgetRepository->findOneBy(['slug' => $slug]);
        if (is_null($budget)) {
            throw $this->createNotFoundException(sprintf('Budget %s not found', $slug));
        }

        $this->denyAccessUnlessGranted('edit', $budget);

        switch ($direction) {
            case 'open':
                $budget->setArchived(false);
                $em->flush();
                $this->addFlash(
                    'success',
                    $translator->trans('budget.archive.open_success', ['%budget%' => $budget->getTitle()], 'loot')
                );
                break;
            case 'close':
                $budget->setArchived(true);
                $em->flush();
                $this->addFlash(
                    'success',
                    $translator->trans('budget.archive.close_success', ['%budget%' => $budget->getTitle()], 'loot')
                );
                break;
            default:
                throw $this->createNotFoundException(sprintf('Bad direction : %s', $direction));
        }

        return $this->redirectToRoute('app_budget');
    }

    /**
     * Delete a contribution.
     */
    #[Route(path: '/budget/log/{id}/delete', name: 'app_budget_contribution_delete')]
    public function deleteLog($id, TranslatorInterface $translator, ContributionRepository $contributionRepository,
        EntityManagerInterface $em
    ): Response {
        /** @var Budget $budget */
        $contribution = $contributionRepository->find($id);
        if (is_null($contribution)) {
            throw $this->createNotFoundException(sprintf('Contribution %s not found', $id));
        }

        $this->denyAccessUnlessGranted('delete', $contribution);

        if (!is_null($contribution->getRetribution())) {
            $this->addFlash(
                'error',
                $translator->trans('contribution.delete.already_claimed', [], 'loot')
            );
        }

        $em->remove($contribution);
        $em->flush();
        $this->addFlash(
            'success',
            $translator->trans('contribution.delete.success', [], 'loot')
        );

        return $this->redirectToRoute('app_budget_history', ['slug' => $contribution->getBudget()->getSlug()]);
    }

    /**
     * Filter contributions with an activities list.
     */
    private function filterByActivities($contributions, $activities_filter): array
    {
        return array_filter($contributions, function ($c) use ($activities_filter) {
            // get an array of activity titles
            $contribution_activities_title = array_map(
                fn ($a) => $a->getTitle(),
                $c->getActivities()->toArray()
            );

            // compute intersect -> if intersect exist, keep the contribution
            return count(array_intersect($activities_filter, $contribution_activities_title)) > 0;
        }
        );
    }
}

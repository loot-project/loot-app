<?php

namespace App\DataFixtures\Processor;

use App\Entity\Budget;
use Doctrine\ORM\EntityManagerInterface;
use Fidry\AliceDataFixtures\ProcessorInterface;
use Ramsey\Uuid\Doctrine\UuidGenerator;

class BudgetProcessor implements ProcessorInterface
{
    protected $om;

    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->om = $objectManager;
    }

    public function preProcess(string $fixtureId, $object): void
    {
        if (false === $object instanceof Budget) {
            return;
        }

        // Fix if referent is not a valid contributor
        if (!$object->getContributors()->contains($object->getReferent())) {
            $object->setReferent($object->getContributors()->first());
        }

        $uuidGenerator = new UuidGenerator();
        $object->setSlug($uuidGenerator->generate($this->om, $object));
    }

    public function postProcess(string $fixtureId, $object): void
    {
        // do nothing
    }
}

<?php

namespace App\DataFixtures\Processor;

use App\Entity\Contribution;
use Doctrine\ORM\EntityManagerInterface;
use Fidry\AliceDataFixtures\ProcessorInterface;

class ContributionProcessor implements ProcessorInterface
{
    protected $om;

    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->om = $objectManager;
    }

    public function preProcess(string $fixtureId, $object): void
    {
        if (false === $object instanceof Contribution) {
            return;
        }

        if (!$object->getBudget()->getContributors()->contains($object->getContributor()->getId())) {
            // Choose a true contributor
            $object->setContributor($object->getBudget()->getContributors()->first());
        }
    }

    public function postProcess(string $fixtureId, $object): void
    {
        // do nothing
    }
}

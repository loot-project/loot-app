<?php

namespace App\DataFixtures\Processor;

use App\Entity\User;
use Fidry\AliceDataFixtures\ProcessorInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserProcessor implements ProcessorInterface
{
    public function __construct(
        private readonly UserPasswordHasherInterface $passwordHasher
    ) {
    }

    public function preProcess(string $fixtureId, $object): void
    {
        if (false === $object instanceof User) {
            return;
        }

        $object->setName(
            $object->getFirstname().' '.strtoupper((string) $object->getLastname())
        );
        $object->setPassword(
            $this->passwordHasher->hashPassword($object, $object->getPassword())
        );
    }

    public function postProcess(string $fixtureId, $object): void
    {
        // do nothing
    }
}

<?php

namespace App\DataFixtures\Processor;

use App\Doctrine\RetributionReferenceGenerator;
use App\Entity\Budget;
use App\Entity\Retribution;
use Doctrine\ORM\EntityManagerInterface;
use Fidry\AliceDataFixtures\ProcessorInterface;

class RetributionProcessor implements ProcessorInterface
{
    protected $om;

    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->om = $objectManager;
    }

    public function preProcess(string $fixtureId, $object): void
    {
        if (false === $object instanceof Retribution) {
            return;
        }

        // Fix budget and contributor of the retribution. Work only if one random contribution in the retribution
        foreach ($object->getContributions() as $contribution) {
            if ($contribution->getBudget() !== $object->getBudget()) {
                // Fix the budget of the retribution
                $object->setBudget($contribution->getBudget());
            }
            if ($contribution->getContributor() !== $object->getContributor()) {
                // Fix the user of the retribution
                $object->setContributor($contribution->getContributor());
            }
        }

        // Generate the reference
        $generator = new RetributionReferenceGenerator();
        $object->setReference($generator->generate($this->om, $object));
    }

    public function postProcess(string $fixtureId, $object): void
    {
        // do nothing
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 14/01/20
 * Time: 17:34.
 */

namespace App\Test;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\Security\Core\User\InMemoryUser;
use Symfony\Component\Security\Core\User\UserInterface;

trait LoginTestCaseTrait
{
    /**
     * @param string $username Use root to log in as root
     * @param array  $roles    Default : ROLE_USER
     *
     * @return string|User return username or a user object loaded from database
     *
     * @throws
     */
    private function logIn(KernelBrowser $client, string $username = 'user', array $roles = ['ROLE_USER']): UserInterface
    {
        if ($username == 'root') {
            $user = new InMemoryUser('root', 'root', ['ROLE_SUPER_ADMIN']);
        } else {
            // create or load user
            $user = $client->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository(User::class)
                              ->findOneBy(['email' => $username.'@localhost']);
            if (is_null($user)) {
                $user = new User();
                $user->setEmail($username.'@localhost');
                $user->setName($user->getEmail());
                $user->setPassword('p@ssword');
                $user->setRoles($roles);
                $em = $client->getContainer()->get('doctrine.orm.default_entity_manager');
                $em->persist($user);
                $em->flush();
            }
        }
        $client->loginUser($user);

        return $user;
    }
}

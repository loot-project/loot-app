<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 14/01/20
 * Time: 17:34.
 */

namespace App\Test\Entity;

use App\Entity\Activity;
use App\Entity\Budget;
use App\Entity\Contribution;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;

/**
 * Help for budget test with doctrine entities.
 */
trait BudgetTestTrait
{
    private function joinBudget(User $user, Budget $budget, ObjectManager $manager): void
    {
        // Add user to budgets
        $budget->addContributor($user);
        $user->addBudget($budget);
        $manager->flush();
    }

    private function setReferent(User $user, Budget $budget, ObjectManager $manager): void
    {
        // Add user to budgets
        $budget->setReferent($user);
        $manager->persist($budget);
        $manager->flush();
    }

    /**
     * @return User
     */
    private function addUser($username, ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail($username.'@localhost');
        $user->setName($user->getEmail());
        $user->setPassword('p@ssword');
        $manager->persist($user);
        $manager->flush();

        return $user;
    }

    /**
     * @return Activity
     */
    private function createActivity($title, ObjectManager $om)
    {
        $activity = new Activity();
        $activity->setTitle(strtolower((string) $title));
        $om->persist($activity);
        $om->flush();

        return $activity;
    }

    /**
     * Generate contributions for the user.
     *
     * If budget has an amount, generate by default contributions to use all the budget
     *
     * @param int $count
     *
     * @return Contribution[]
     *
     * @throws \Exception
     */
    private function generateContributions(User $user, Budget $budget, ObjectManager $em, $count = 3)
    {
        $contributions = [];
        for ($i = 1; $i <= $count; ++$i) {
            $day_diff = mt_rand(5, 10);
            $created_at = new \DateTime();
            $created_at->sub(new \DateInterval('P'.$day_diff.'D'));
            $doned_at = new \DateTime();
            $doned_at->sub(new \DateInterval('P'.($day_diff - 1).'D'));
            $c = new Contribution();
            $c->setContributor($user);
            $c->setBudget($budget);
            if ($budget->isTimetracking()) {
                $c->setDuration(mt_rand(1, 10));
            }
            $c->setCreated($created_at);
            $c->setDoned($doned_at);
            if (!is_null($budget->getAmount())) {
                $max_amount = floor($budget->getAmount() / $count);
                $c->setAmount($max_amount);
            }
            $c->setDescription('Description of the contribution');
            $em->persist($c);
            $contributions[] = $c;
        }
        $em->flush();

        return $contributions;
    }
}

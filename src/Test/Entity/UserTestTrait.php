<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 14/01/20
 * Time: 17:34.
 */

namespace App\Test\Entity;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;

/**
 * User helper for User entity.
 */
trait UserTestTrait
{
    /**
     * Create and flush.
     *
     * @return User
     */
    protected function createUser($username, ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail($username.'@localhost');
        $user->setName($user->getEmail());
        $user->setPassword('p@ssword');
        $manager->persist($user);
        $manager->flush();

        return $user;
    }
}

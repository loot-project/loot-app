<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 28/01/20
 * Time: 12:17.
 */

namespace App\Test\Entity\Loader;

use App\Entity\Budget;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Doctrine\UuidGenerator;

class BudgetLoader
{
    protected $budgets = [];

    public function __construct()
    {
        $budget = new Budget();

        $budget->setTitle('Test');
        $budget->setSubtitle('Budget for unit et functionnal test');
        $budget->setAmount(1000);
        $budget->setTimetracking(true);
        $budget->setUnit('euros');
        $budget->setUnitSymbol('€');

        $this->budgets[] = $budget;
    }

    public function load(ObjectManager $em): array
    {
        foreach ($this->budgets as $budget) {
            $uuidGenerator = new UuidGenerator();
            $budget->setSlug($uuidGenerator->generate($em, $budget));
            $em->persist($budget);
        }
        $em->flush();

        return $this->budgets;
    }

    public function addBudgetWithoutTimeTracking($title = 'No timetracking Budget', $budget_amount = null): void
    {
        $budget = new Budget();
        $budget->setTitle($title);
        $budget->setSubtitle($title.' subtitle');
        $budget->setTimetracking(false);
        if ($budget_amount > 0) {
            $budget->setAmount(1000);
            $budget->setUnit('euros');
            $budget->setUnitSymbol('€');
        }
        $this->budgets[] = $budget;
    }

    public function addBudgetWithRules($url_rules = 'http://rules.com/budget', $title = 'Budget wiht rules', $budget_amount = null): void
    {
        $budget = new Budget();
        $budget->setTitle($title);
        $budget->setSubtitle($title.' subtitle');
        $budget->setTimetracking(false);
        if ($budget_amount > 0) {
            $budget->setAmount(1000);
            $budget->setUnit('euros');
            $budget->setUnitSymbol('€');
        }
        $budget->setRules($url_rules);
        $this->budgets[] = $budget;
    }
}

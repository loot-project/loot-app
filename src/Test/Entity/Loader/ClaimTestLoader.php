<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 28/01/20
 * Time: 12:17.
 */

namespace App\Test\Entity\Loader;

use App\Entity\Budget;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Doctrine\UuidGenerator;

/**
 * Class to generate data required for a claim.
 */
class ClaimTestLoader extends Fixture
{
    protected $budgets = [];

    public function __construct()
    {
        $budget = new Budget();

        $budget->setTitle('Test');
        $budget->setSubtitle('Budget for unit et functionnal test');
        $budget->setAmount(1000);
        $budget->setTimetracking(true);
        $budget->setUnit('euros');
        $budget->setUnitSymbol('€');

        $this->budgets[] = $budget;
    }

    public function load(ObjectManager $em): array
    {
        foreach ($this->budgets as $budget) {
            $uuidGenerator = new UuidGenerator();
            $budget->setSlug($uuidGenerator->generate($em, $budget));
            $em->persist($budget);
        }
        $em->flush();

        return $this->budgets;
    }
}

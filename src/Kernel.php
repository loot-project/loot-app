<?php

namespace App;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;

class Kernel extends BaseKernel
{
    use MicroKernelTrait {
        MicroKernelTrait::registerBundles as registerBundlesBase;
    }

    public function registerBundles(): iterable
    {
        yield from $this->registerBundlesBase();

        $plugins = [];
        $dirs = array_filter(glob(__DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'plugins'.DIRECTORY_SEPARATOR.'*'), 'is_dir');
        foreach ($dirs as $plugin) {
            $plugin_tree = explode(DIRECTORY_SEPARATOR, $plugin);
            $plugin_name = end($plugin_tree);
            $plugins['Loot\\Plugins\\'.$plugin_name.'\\'.$plugin_name] = ['all' => true];
        }

        foreach ($plugins as $class => $envs) {
            if ($envs[$this->environment] ?? $envs['all'] ?? false) {
                yield new $class();
            }
        }
    }
}

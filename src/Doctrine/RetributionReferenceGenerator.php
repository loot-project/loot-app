<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 17/02/20
 * Time: 15:18.
 */

namespace App\Doctrine;

use App\Entity\Retribution;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;

class RetributionReferenceGenerator extends AbstractIdGenerator
{
    /**
     * Generate an identifier.
     *
     * @param \Doctrine\ORM\Mapping\Entity $entity
     *
     * @return string
     *
     * @throws \Exception Bad format for last retribution reference
     */
    public function generate(EntityManager $em, $entity)
    {
        $date_time = new \DateTime();

        // Get last retribution for increment
        /** @var Retribution $last_retribution */
        $last_retribution = $em->getRepository(Retribution::class)->lastRetribution();
        if (is_null($last_retribution)) {
            return 'LT-'.$date_time->format('Y').'-000001';
        }

        $parts = [];
        if (!preg_match('/^LT-([0-9]{4})-([0-9]{6})$/', (string) $last_retribution->getReference(), $parts)) {
            throw new \Exception('Bad format for last retribution reference');
        }

        if ($date_time->format('Y') == $parts[1]) {
            $index = intval(end($parts));
        } else {
            $index = 0;
        }

        return 'LT-'.$date_time->format('Y').'-'.str_pad($index + 1, 6, '0', STR_PAD_LEFT);
    }
}

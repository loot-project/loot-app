<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 27/02/20
 * Time: 09:30.
 */

namespace App\Twig;

use App\Entity\Budget;
use App\Repository\ContributionRepository;
use App\Repository\RetributionRepository;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class BudgetTwigExtension extends AbstractExtension
{
    public function __construct(
        private readonly Environment $twig,
        private readonly RetributionRepository $retributionRepository,
        private readonly ContributionRepository $contributionRepository
    ) {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('budget_bar', $this->renderBudgetBar(...)),
        ];
    }

    public function renderBudgetBar(Budget $budget): void
    {
        $total_amount_claimed = $this->retributionRepository->totalAmountClaimed($budget->getSlug());
        $total_amount_logged = $this->contributionRepository->totalAmountLogged($budget->getSlug());
        if (is_null($budget->getAmount())) {
            return;
        }
        echo $this->twig->render('partials/_budget_progress_bar.html.twig', [
            'budget' => $budget, 'total_amount_claimed' => $total_amount_claimed,
            'total_amount_logged' => $total_amount_logged,
        ]);
    }
}

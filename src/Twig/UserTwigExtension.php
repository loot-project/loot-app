<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 27/02/20
 * Time: 09:30.
 */

namespace App\Twig;

use App\Entity\User;
use Symfony\Component\Security\Core\Security;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class UserTwigExtension extends AbstractExtension
{
    public function __construct(private readonly Security $security)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('is_regular_loot_user', $this->isLoggedUserFromRegularLootUser(...)),
        ];
    }

    public function isLoggedUserFromRegularLootUser()
    {
        return !is_null($this->security->getUser()) && $this->security->getUser() instanceof User;
    }
}

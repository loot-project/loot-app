<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 27/02/20
 * Time: 09:30.
 */

namespace App\Twig;

use App\Security\UserVoter;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class SecurityTwigExtension extends AbstractExtension
{
    public function __construct(private readonly UserVoter $userVoter)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('is_delete_denied_by_existing_contributions', $this->userVoter->isDeleteDeniedByExistingContributions(...)),
            new TwigFunction('is_delete_denied_by_budget_membership', $this->userVoter->isDeleteDeniedByBudgetMembership(...)),
        ];
    }
}

<?php

namespace App\Repository;

use App\Entity\Budget;
use App\Entity\Contribution;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Contribution|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contribution|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contribution[]    findAll()
 * @method Contribution[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContributionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contribution::class);
    }

    /**
     * Return contribution of a user available for a retribution claim.
     *
     * @return Contribution[]
     */
    public function getContributionsToClaim(UserInterface $user, Budget $budget)
    {
        $qb = $this->createQueryBuilder('c');

        return $qb
            ->andWhere('c.contributor = :user')
            ->andWhere($qb->expr()->isNull('c.retribution'))
            ->andWhere('c.budget=:budget')
            ->setParameter('user', $user)->setParameter('budget', $budget)
            ->orderBy('c.doned', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Return true if user has contribution for the budget.
     *
     * @return bool
     *
     * @throws NonUniqueResultException
     */
    public function isUserHasContributionsForBudgetToClaim(UserInterface $user, Budget $budget)
    {
        $qb = $this->createQueryBuilder('c')->andWhere('c.contributor = :user')->andWhere('c.budget=:budget');

        return !is_null(
            $qb->andWhere($qb->expr()->isNull('c.retribution'))->setParameter('user', $user)->setParameter(
                'budget', $budget
            )->setMaxResults(1)->getQuery()->getOneOrNullResult()
        );
    }

    /**
     * @return int
     *
     * @throws NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function countContributions(Budget $budget)
    {
        return $this->createQueryBuilder('c')
                    ->select('COUNT(c.id) as total')
                   ->andWhere('c.budget=:budget')
            ->setParameter('budget', $budget)
            ->getQuery()->getSingleScalarResult();
    }

    /**
     * @throws NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function totalAmountLogged($budget_slug)
    {
        return $this->createQueryBuilder('c')
                    ->select('SUM(c.amount) as total')
                    ->innerJoin('c.budget', 'b')
                    ->where('b.slug=:slug')->andWhere('c.amount>0')
                    ->setParameter('slug', $budget_slug)
                    ->getQuery()->getSingleScalarResult();
    }

    // /**
    //  * @return Contribution[] Returns an array of Contribution objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Contribution
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

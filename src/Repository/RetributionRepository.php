<?php

namespace App\Repository;

use App\Entity\Budget;
use App\Entity\Retribution;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Retribution|null find($id, $lockMode = null, $lockVersion = null)
 * @method Retribution|null findOneBy(array $criteria, array $orderBy = null)
 * @method Retribution[]    findAll()
 * @method Retribution[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RetributionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Retribution::class);
    }

    /**
     * @return Retribution[]
     */
    public function findByUser(User $user)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.contributor= :user_id')
            ->setParameter('user_id', $user->getId())
            ->orderBy('r.reference', 'DESC')
            ->getQuery()->getResult();
    }

    /**
     * @return Retribution[]
     */
    public function findByBudget(Budget $budget)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.budget= :budget')
            ->setParameter('budget', $budget)
            ->orderBy('r.reference', 'DESC')
            ->getQuery()->getResult();
    }

    /**
     * @return Retribution
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function lastRetribution()
    {
        return $this->createQueryBuilder('r')->setMaxResults(1)->orderBy('r.created', 'desc')
            ->getQuery()->getOneOrNullResult();
    }

    /**
     * @return int
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function totalAmountClaimed($budget_slug)
    {
        return $this->createQueryBuilder('r')
                    ->select('SUM(c.amount) as total')
                    ->innerJoin('r.contributions', 'c')
                    ->innerJoin('c.budget', 'b')
                    ->where('b.slug=:slug')->andWhere('c.amount>0')
                    ->setParameter('slug', $budget_slug)
                    ->getQuery()->getSingleScalarResult();
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 17/02/20
 * Time: 14:54.
 */

namespace App\Entity\Manager;

use App\Doctrine\RetributionReferenceGenerator;
use App\Entity\Budget;
use App\Entity\Contribution;
use App\Entity\Retribution;
use App\Entity\User;
use App\Exception\AccessDeniedException;
use App\Service\ContributorMailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class BudgetManager
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ContributorMailer $mailer,
        private readonly TranslatorInterface $translator,
        private readonly Environment $twig
    ) {
    }

    /**
     * @return Retribution|null Return the retribution created or null if no action done
     *
     * @throws AccessDeniedException Contribution not related to the user
     */
    public function claim(User $user, array $contributions)
    {
        $retribution = new Retribution();
        $retribution->setContributor($user);

        // Check if contribution related to user
        /** @var Contribution $c */
        foreach ($contributions as $c) {
            if ($c->getContributor()->getId() != $user->getId()) {
                throw new AccessDeniedException();
            }
            if (!is_null($c->getRetribution())) {
                continue;
            }
            $retribution->addContribution($c);
            $c->setRetribution($retribution);
        }

        if (count($retribution->getContributions()) <= 0) {
            return null;
        }

        $budget = current($contributions)->getBudget();
        $retribution->setBudget($budget);

        // Generate the reference
        $generator = new RetributionReferenceGenerator();
        $retribution->setReference($generator->generate($this->em, $retribution));

        // send emails to the referent of the budget and to the contributor
        $vars = ['contributions' => $retribution->getContributions(),
            'contributor' => $retribution->getContributor()->getName(),
            'budget' => $budget,
            'reference' => $retribution->getReference(),
            'amount' => $this->contributionTotalAmount($retribution->getContributions()),
        ];

        $subject = $this->translator->trans('claim.notification_subject', [], 'loot');
        $mail_html = $this->twig->render('budget/email_claim_notification.html.twig', $vars);
        $mail_txt = $this->twig->render('budget/email_claim_notification.txt.twig', $vars);

        if (!is_null($budget->getReferent()) && $budget->getReferent() != $retribution->getContributor()) {
            $this->mailer->sendMail($budget->getReferent(), $subject, $mail_txt, $mail_html);
        }
        $this->mailer->sendMail($retribution->getContributor(), $subject, $mail_txt, $mail_html);

        return $retribution;
    }

    /**
     * @param array $contributions
     *
     * @return int Return the total amount of contributions
     */
    public function contributionTotalAmount($contributions)
    {
        $totalAmount = 0;
        foreach ($contributions as $contribution) {
            $totalAmount += $contribution->getAmount();
        }

        return $totalAmount;
    }

    /**
     * User join a budget as contributor.
     *
     * Return null if already joined or true if joined. Flush the entity manager after calling this method.
     *
     * @param User $user
     * @param Budget $budget
     */
    public function join(UserInterface $user, Budget $budget)
    {
        // Check if already joined
        if ($budget->getContributors()->contains($user)) {
            return null;
        }

        // Join
        $budget->getContributors()->add($user);
        return true;
    }
}

<?php

namespace App\Entity\Manager;

use App\Repository\ActivityRepository;
use Doctrine\ORM\EntityManagerInterface;

class ActivityManager
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ActivityRepository $activityRepository
    ) {
    }

    /**
     * Remove all activities unused bu budgets.
     *
     * @param array $exceptions List of activities to keep
     */
    public function removeUnusedActivity(
        $exceptions = []
    ): void {
        $activities = $this->activityRepository->findActivitesUnused();
        foreach ($activities as $activity) {
            if (in_array($activity, $exceptions)) {
                continue;
            }
            $this->em->remove($activity);
        }
    }
}

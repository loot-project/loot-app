<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 04/02/20
 * Time: 16:50.
 */

namespace App\Security;

use App\Entity\Budget;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class AccessDeniedHandler implements AccessDeniedHandlerInterface
{
    protected $translator;
    protected $twig;

    public function __construct(TranslatorInterface $translator, Environment $twig)
    {
        $this->translator = $translator;
        $this->twig = $twig;
    }

    public function handle(Request $request, AccessDeniedException $accessDeniedException): ?Response
    {
        $response = new Response();

        $subject = $accessDeniedException->getSubject();
        if (!is_null($subject) && $subject instanceof Budget) {
            $content = $this->twig->render(
                'security/denied.html.twig', [
                    'message' => $this->translator->trans(
                        'budget.access_denied', ['%budget%' => $subject->getTitle()], 'loot'
                    ),
                ]
            );
        } else {
            $content = $this->twig->render(
                'security/denied.html.twig', [
                    'message' => $this->translator->trans('access_denied', [], 'loot'),
                ]
            );
        }
        $response->setContent($content);

        return $response;
    }
}

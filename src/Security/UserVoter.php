<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 04/02/20
 * Time: 16:41.
 */

namespace App\Security;

use App\Entity\User;
use App\Repository\BudgetRepository;
use App\Repository\ContributionRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Voter to check access rights on user edit.
 */
class UserVoter extends Voter
{
    public const DELETE = 'delete';
    public const VIEW = 'view';
    public const EDIT = 'edit';

    protected function supports($attribute, $subject): bool
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, [self::VIEW, self::EDIT, self::DELETE])) {
            return false;
        }

        // only vote on Post objects inside this voter
        if (!$subject instanceof User) {
            return false;
        }

        return true;
    }

    public function __construct(
        private readonly ContributionRepository $contributionRepository,
        private readonly BudgetRepository $budgetRepository
    ) {
    }

    /**
     * @param string $attribute
     * @param User   $contributor
     *
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, mixed $entity, TokenInterface $token): bool
    {
        if (!$this->isLogged($token)) {
            // the user must be logged in; if not, deny access
            return false;
        }

        $is_edit_access = $this->isRoot($token)
            || ($token->getUser() instanceof User && $token->getUser()->getId() == $entity->getId());

        return match ($attribute) {
            self::VIEW => true,
            self::DELETE => $is_edit_access && !$this->isDeleteDeniedByBudgetMembership($entity)
                && !$this->isDeleteDeniedByExistingContributions($entity),
            default => $is_edit_access,
        };
    }

    public function isDeleteDeniedByExistingContributions(User $user)
    {
        return count($this->contributionRepository->findBy(['contributor' => $user])) > 0;
    }

    public function isDeleteDeniedByBudgetMembership(User $user)
    {
        return count($this->budgetRepository->findByUser($user)) > 0;
    }
}

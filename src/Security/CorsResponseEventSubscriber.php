<?php

namespace App\Security;

use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class CorsResponseEventSubscriber implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        // return the subscribed events, their methods and priorities
        return [
            KernelEvents::RESPONSE => [
                ['allowOrigin', 10],
            ],
        ];
    }

    /**
     * Allow cross control origin for all if it's a GET/OPTIONS request on the API.
     *
     * We use it instead of NelmioCorsBundle because it does not work. See https://gitlab.com/loot-project/loot-app/-/issues/60
     */
    public function allowOrigin(ResponseEvent $event): void
    {
        if (in_array($event->getRequest()->getMethod(), ['OPTIONS', 'GET'])) {
            if (str_contains($event->getRequest()->getPathInfo(), '/api')) {
                $event->getResponse()->headers->set('Access-Control-Allow-Origin', '*');
            }
        }
    }
}

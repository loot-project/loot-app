<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 04/02/20
 * Time: 16:41.
 */

namespace App\Security;

use App\Entity\Budget;
use App\Entity\Contribution;
use App\Entity\User;
use App\Repository\ContributionRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Voter to check access rights on a budget for the current user.
 */
class BudgetVoter extends Voter
{
    public const CONTRIBUTE = 'contribute';
    public const CREATE = 'create';
    public const EDIT = 'edit';
    public const JOIN = 'join';
    public const DELETE = 'delete';

    public function __construct(
        private readonly ContributionRepository $contributionRepository
    ) {
    }

    protected function supports($attribute, $subject): bool
    {
        // if the attribute isn't one we support, return false
        if ($subject instanceof Budget && in_array($attribute, [self::CONTRIBUTE, self::CREATE, self::EDIT, self::JOIN, self::DELETE])) {
            return true;
        }

        if ($subject instanceof Contribution && in_array($attribute, [self::DELETE])) {
            return true;
        }

        return false;
    }

    /**
     * @param string $attribute
     *
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, mixed $entity, TokenInterface $token): bool
    {
        if (!$this->isLogged($token)) {
            // the user must be logged in; if not, deny access
            return false;
        }

        $budget = ($entity instanceof Budget) ? $entity : null;
        $contribution = ($entity instanceof Contribution) ? $entity : null;

        if (!is_null($contribution)) {
            return !$this->isRoot($token) && $contribution->getContributor() === $token->getUser();
        }

        // Budget
        switch ($attribute) {
            case self::CREATE:
                return !$this->isRoot($token);
            case self::JOIN:
                return !$this->isRoot($token);
            case self::DELETE:
                if (!$this->isRoot($token)) {
                    return $this->contributionRepository->countContributions($budget) <= 0
                        && count($budget->getContributors()) <= 0;
                }

                return $this->isRoot($token);
            default:
                return !$this->isRoot($token) && $this->isContributor($budget, $token->getUser());
        }
    }

    private function isContributor(Budget $budget, User $user)
    {
        return $budget->getContributors()->contains($user);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 04/02/20
 * Time: 16:41.
 */

namespace App\Security;

use App\Entity\Retribution;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Voter to check access rights on a budget for the current user.
 */
class RetributionVoter extends Voter
{
    public const VIEW = 'view';

    protected function supports($attribute, $subject): bool
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, [self::VIEW])) {
            return false;
        }

        // only vote on Post objects inside this voter
        if (!$subject instanceof Retribution) {
            return false;
        }

        return true;
    }

    /**
     * @param string      $attribute
     * @param Retribution $retribution
     *
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, mixed $entity, TokenInterface $token): bool
    {
        if (!$this->isLogged($token)) {
            // the user must be logged in; if not, deny access
            return false;
        }

        return match ($attribute) {
            self::VIEW => $entity->getContributor() === $token->getUser(),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }
}

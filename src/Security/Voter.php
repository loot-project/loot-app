<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 04/02/20
 * Time: 16:41.
 */

namespace App\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter as BaseVoter;

/**
 * Base Voter to offer helper method.
 */
abstract class Voter extends BaseVoter
{
    protected function isLogged(TokenInterface $token)
    {
        return !is_null($token) && !is_null($token->getUser());
    }

    protected function isRoot(TokenInterface $token)
    {
        $user = $token->getUser();

        return (is_object($user) && in_array('ROLE_SUPER_ADMIN', $user->getRoles()))
            || (is_string($user) && $user == 'root');
    }
}

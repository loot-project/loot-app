<?php

/*
 * This file is part of the Loot
 *
 * Author : Florent Kaisser
 *
 * Service to send a mail to a Contributor (User)
 *
 */

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class ContributorMailer
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly MailerInterface $mailer
    ) {
    }

    /**
     * Send a mail to a contributor.
     *
     * @param mixed user Id or User object $user
     * @param string subject of the mail $subject
     * @param string Plain text of mail content $text
     * @param string HTML of mail content $html
     *
     * @throws \OutOfBoundsException     User id does not exist
     * @throws \InvalidArgumentException User must be a Id or a User object
     */
    public function sendMail($user, $subject, $text, $html): void
    {
        if (is_int($user)) {
            $user_obj = $this->userRepository->find($user);
            if (is_null($user_obj)) {
                throw new \OutOfBoundsException("User id {$user} not found");
            }
        } elseif ($user instanceof User) {
            $user_obj = $user;
        } else {
            throw new \InvalidArgumentException('User must be a Id or a User object');
        }

        $email = (new Email())->to($user_obj->getEmail())->subject($subject)->text($text)->html($html);
        $this->mailer->send($email);
    }
}

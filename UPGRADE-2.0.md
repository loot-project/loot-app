UPGRADE FROM 1.x to 2.0
=======================

## Upgrade to 1.1.4

The upgrade to 1.1.4 is required before upgrading to 2.0.0. [follow this guide](UPGRADE-1.1.md)

## Upgrade to 2.0.0

The version 2.0 has new requirements : 

- PHP 8.2
- MariaDB 10.3 is always supported but the version 10.4 is recommanded. Version 11.4 is the version used by default.
- Nodejs 18 with Yarn for building assets

If you use docker-compose, the docker image has changed to package only the app without the web server.
You have to use a web server image like `nginx:alpine` with PHP-FPM like the example in [the Readme.md of the support/docker/production directory](support/docker/production/Readme.md).

### Database upgrade for non docker users

Run migrations :

```bash
php bin/console doctrine:migrations:migrate
``` 

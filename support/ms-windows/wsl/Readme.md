# Run loot on Microsoft Windows with WSL

NB: If you want to use Docker on Windows, you need an Windows License Pro or Entreprise. On Windows Family, Hyper-V is not available, so you can use a basic LAMP install. 

[Install Ubuntu 18.04 LTS for Windows](https://www.microsoft.com/en-us/p/ubuntu/9nblggh4msv6?activetab=pivot:overviewtab)

Launch Ubuntu 18.04 LTS on your Windows system.

Follow instructions to create new user.

Then execute :

```bash
sudo apt-get update
sudo apt-get upgrade
```

Install LAMP :

```bash
sudo apt install apache2 php libapache2-mod-php mysql-server php-mysql
```

Install Nodejs :

```bash
sudo apt-get install nodejs npm
```

Install Composer :

```bash
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
php -r "unlink('composer-setup.php');"
```

Install Yarn :

```bash
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update && sudo apt install yarn
sudo apt update && sudo apt install --no-install-recommends yarn
```

Create a Virtual Host on Apache for the app : 

```bash
cd /etc/apache2/sites-available/
sudo nano loot.conf
```

You can use this basic configuration file :

```
<VirtualHost *:80>
    ServerName loot.local

    DocumentRoot [Chemin vers votre dossier d'installation]

    ErrorLog ${APACHE_LOG_DIR}/error.loot.local.log
    CustomLog ${APACHE_LOG_DIR}/access.loot.local.log combined

    <Directory [Chemin vers votre dossier d'installation]>
        Options Indexes FollowSymLinks
        AllowOverride all
        Require all granted
    </Directory>
</VirtualHost>
```

Ubuntu for Windows mount your windows disks on /mnt/. For example, to access the "loot" folder of your D: drive, use /mnt/d/loot/

Enable Apache module and restart :

```bash
sudo a2enmod rewrite
sudo service apache2 restart
```

Edit your local host file :

- Open Notepad with admin rights
- Open file :  C:\Windows\System32\drivers\etc\hosts
- At the end, add : ```127.0.0.1   loot.local``` 


To clone the app, go to your document root folder and clone : 

```bash
git clone https://gitlab.com/loot-project/loot-app.git loot-app
```

Add necessary extensions:

```bash
sudo apt-get install php-xml php-curl php-zip php-intl php-pdo-mysql php-xdebug
```

Install the dependencies :

```bash
composer install
yarn install
```

Generate the app assets :

```bash
yarn encore dev
```

If the apache-pack symfony module failed to create the .htaccess in the /public/ folder, create one with the content below : 

```bash
# Use the front controller as index file. It serves as a fallback solution when
# every other rewrite/redirect fails (e.g. in an aliased environment without
# mod_rewrite). Additionally, this reduces the matching process for the
# start page (path "/") because otherwise Apache will apply the rewriting rules
# to each configured DirectoryIndex file (e.g. index.php, index.html, index.pl).
DirectoryIndex index.php

# By default, Apache does not evaluate symbolic links if you did not enable this
# feature in your server configuration. Uncomment the following line if you
# install assets as symlinks or if you experience problems related to symlinks
# when compiling LESS/Sass/CoffeScript assets.
# Options FollowSymlinks

# Disabling MultiViews prevents unwanted negotiation, e.g. "/index" should not resolve
# to the front controller "/index.php" but be rewritten to "/index.php/index".
<IfModule mod_negotiation.c>
    Options -MultiViews
</IfModule>

<IfModule mod_rewrite.c>
    RewriteEngine On

    # Determine the RewriteBase automatically and set it as environment variable.
    # If you are using Apache aliases to do mass virtual hosting or installed the
    # project in a subdirectory, the base path will be prepended to allow proper
    # resolution of the index.php file and to redirect to the correct URI. It will
    # work in environments without path prefix as well, providing a safe, one-size
    # fits all solution. But as you do not need it in this case, you can comment
    # the following 2 lines to eliminate the overhead.
    RewriteCond %{REQUEST_URI}::$0 ^(/.+)/(.*)::\2$
    RewriteRule .* - [E=BASE:%1]

    # Sets the HTTP_AUTHORIZATION header removed by Apache
    RewriteCond %{HTTP:Authorization} .+
    RewriteRule ^ - [E=HTTP_AUTHORIZATION:%0]

    # Redirect to URI without front controller to prevent duplicate content
    # (with and without `/index.php`). Only do this redirect on the initial
    # rewrite by Apache and not on subsequent cycles. Otherwise we would get an
    # endless redirect loop (request -> rewrite to front controller ->
    # redirect -> request -> ...).
    # So in case you get a "too many redirects" error or you always get redirected
    # to the start page because your Apache does not expose the REDIRECT_STATUS
    # environment variable, you have 2 choices:
    # - disable this feature by commenting the following 2 lines or
    # - use Apache >= 2.3.9 and replace all L flags by END flags and remove the
    #   following RewriteCond (best solution)
    RewriteCond %{ENV:REDIRECT_STATUS} =""
    RewriteRule ^index\.php(?:/(.*)|$) %{ENV:BASE}/$1 [R=301,L]

    # If the requested filename exists, simply serve it.
    # We only want to let Apache serve files and not directories.
    # Rewrite all other queries to the front controller.
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ %{ENV:BASE}/index.php [L]
</IfModule>

<IfModule !mod_rewrite.c>
    <IfModule mod_alias.c>
        # When mod_rewrite is not available, we instruct a temporary redirect of
        # the start page to the front controller explicitly so that the website
        # and the generated links can still be used.
        RedirectMatch 307 ^/$ /index.php/
        # RedirectTemp cannot be used instead
    </IfModule>
</IfModule>
```

Now you should be able to access your Loot instance by visiting  http://loot.local 
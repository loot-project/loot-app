# Loot production Dockerfile

## Deploy with docker-compose

### First installation

In the directory [support/docker/production](.) you will find the

- [docker-compose.yml.dist](docker-compose.yml.dist) file to deploy the application in production
- [config/default.conf.template](config/default.conf.temaplate) used by the webserver nginx to plug the loot app as php-fpm

Copy the files in production and configure your docker-compose as you want.

And run : `docker-compose -f docker-compose.yml -p "loot-prod" up -d`

The loot application initializes the database if it is the first installation or upgrade it if it is an existing installation.

### Upgrade

1. Check the difference between your docker-compose.yml file and the new one to recreate your deployment stack.
2. Backup your database
3. Stop the running containers
4. Run the new containers

Before upgrading from version 1.x to 2+, you must upgrade [to the last version 1.1.4](https://gitlab.com/loot-project/loot-app/-/releases/v1.1.4) and manually migrate the database.
Read the [UPGRADE-1.1.md](../../../UPGRADE-1.1.md) file to know the steps to upgrade the database.

Then you will be able to upgrade to 2.x or higher.

## Maintainer : build and publish the image

### Build the image

```bash
# build from local repo
cd /path/to/loot
docker build -t local/loot \
  -f support/docker/production/Dockerfile \
  --target loot \
  "`pwd`"
```

```bash
cd /path/to/loot
# Choose your version - tag or branch name
LOOT_VERSION="v2.0.0"
LOOT_BRANCH="release/$LOOT_VERSION"
# OR LOOT_VERSION="latest" only for the branch master
# build from gitlap repo
docker build -t local/loot \
  --build-arg LOOT_GITLAB_REPO_URI=https://gitlab.com/loot-project/loot-app.git \
  --build-arg SOURCE=gitlab \
    --build-arg LOOT_BRANCH=$LOOT_BRANCH \
  -f support/docker/production/Dockerfile \
  --target loot .
```

### Publish the image to registry.gitlab.com/loot-project/loot-app:<tag>

```bash
docker tag local/loot registry.gitlab.com/loot-project/loot-app:$LOOT_VERSION
docker push registry.gitlab.com/loot-project/loot-app:$LOOT_VERSION
```

### Test the image

Copy the [docker-compose.yml.dist](docker-compose.yml.dist) file as docker-compose.yml and configure as you want to use the image local/loot

Run image to test it :

```bash
cd /path/to/loot
cp support/docker/production/docker-compose.yml.dist support/docker/production/docker-compose.yml
docker-compose -f support/docker/production/docker-compose.yml -p "loot-prod" down -v
docker-compose -f support/docker/production/docker-compose.yml -p "loot-prod" up -d
```
#!/bin/bash

env=dev
if [[ ! -z "$1" ]]; then
    env="$1"
fi

docker exec -it loot.nodejs bash -c "yarn run encore ${env}"

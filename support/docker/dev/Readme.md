# Docker Compose support to setup a developpment environment

## Requirements

- [Docker](https://docs.docker.com/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)
- Ubuntu, MacOS X, Ubuntu in Windows with WSL2

## Setup and up docker containers

Clone the repository (need to add SSH key to your Gitlab account)

```
git clone git@gitlab.com:loot-project/loot-app.git loot
mkdir loot/var
```

Copy and past the files ```*.dist``` and configure as you need

```bash
cp loot/support/docker/dev/docker-compose.yml.dist docker-compose.yml
```

Build the dev image

```bash
docker-compose build
```

Then, you change the use id with your user id (`id -u` to get it) in the docker-compose.yml file.
It's important to keep the same user id beetween your host and the one running the container for the app (to avoid permission issues).

Up the containers

```bash
docker-compose up -d
```

## Install loot

The guide has been written from the [production install guide](../../../docs/install.md)
and the [developpers guide](../../../docs/developpers.md).
Check the guides in case of you have some problems : commons errors and instructions may missed here.

### PHP App

From the root directory of the app, set up environnement variables by creating a ```.env.local``` file from ```.env```

Example of a .env.local file :

```dotenv
APP_ROOT_PASSWORD='myP@ssw0rd'
APP_LOCALE=fr
DATABASE_URL=mysql://root:p_ssword@db:3306/loot?mariadb-10.3.11
MAILER_DSN=smtp://user:password@smtp.domain.com:465
MAILER_SENDER=you@domain.com
```

Build the application (we consider that your user id in Ubuntu)

```bash
docker exec -it -u `id -u` loot.webserver bash -c 'composer install'
```

### Assets

```bash
docker exec -it -u `id -u` loot.nodejs bash -c 'yarn install'
docker exec -it -u `id -u` loot.nodejs bash -c 'yarn encore dev'
```

### Database

Configure the database connexion by copying the database URL example from .env to .env.local and create it if does not
exist.

```bash
docker exec -it -u `id -u` loot.webserver bash -c 'php bin/console doctrine:database:create'
docker exec -it -u `id -u` loot.webserver bash -c 'php bin/console doctrine:schema:update --force'
docker exec -it -u `id -u` loot.webserver bash -c 'php bin/console doctrine:migrations:version --add --all'
```

Fill the database with fixtures :

```bash
docker exec -it -u `id -u` loot.webserver bash -c 'php bin/console hautelook:fixtures:load'
```

After setup, browse your app : ```http://<ip of loot.webserver container>```

## Run test

```
docker exec -it -u www-data loot.webserver bash -c 'php bin/phpunit'
```

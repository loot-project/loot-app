# Loot Demo

## Build

```bash
# build from local repo
cd /path/to/loot
docker build -t local/loot-demo \
  -f support/docker/demo/Dockerfile \
  --target loot \
  "`pwd`"
```

```bash
cd /path/to/loot-demo
# build from gitlap repo
docker build -t local/loot-demo \
  --build-arg LOOT_GITLAB_REPO_URI=https://gitlab.com/loot-project/loot-app.git \
  --build-arg SOURCE=gitlab \
  -f support/docker/demo/Dockerfile \
  --target loot .
```

## Publish

Build before publish !

```
docker login registry.gitlab.com
docker tag local/loot-demo registry.gitlab.com/loot-project/loot-app:demo
docker push registry.gitlab.com/loot-project/loot-app:demo
```

## Run

Copy the [support/docker/demo/docker-compose.yml.dist](support/docker/demo/docker-compose.yml.dist)
as `support/docker/demo/docker-compose.yml` and configure as you want. Run

```bash
cd /path/to/loot-demo
docker-compose -f support/docker/demo/docker-compose.yml -p "loot-demo" down -v
docker-compose -f support/docker/demo/docker-compose.yml -p "loot-demo" up -d
```

## Deploy (on Soletic)

```
cd /vps/loot-demo
vps-compose stop && vps-compose rm && docker volume rm lootdemo_app-public
docker pull registry.gitlab.com/loot-project/loot-app:demo
vps-compose up
```


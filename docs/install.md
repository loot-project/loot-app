# Basic instructions to install Loot

The following guide explains step by step how to configure, build and run the application. You have to adapt
to adapt it to your particular case. In the source directory [support](../support) you can browse tutorials for specific
environment. Examples :

- [Docker Compose support to setup a developpment environment](../support/docker/dev/Readme.md)
- [Docker Compose support to setup a production environment](../support/docker/production/Readme.md)
- [Run loot on Microsoft Windows with WSL](../support/ms-windows/wsl/Readme.md)

## Requirements

- Git
- PHP 8.2+ and [minimal requirements of Symfony 6.4](https://symfony.com/doc/current/setup.html#technical-requirements)
- Nodejs 18 with Yarn for building assets
- An available SQL Server like MariaDB.

## Configure and build app

If you run an existing installation, read the [upgrade notes](upgrade.md).

Clone the app

```
git clone https://gitlab.com/loot-project/loot-app ./loot
cd loot
```

Set up environnement variables by creating a ```.env.local``` file from ```.env```.

Example of a .env.local file :

```
APP_ROOT_PASSWORD='myP@ssw0rd'
APP_LOCALE=fr
DATABASE_URL=mysql://root:p_ssword@db:3306/loot?serverVersion=11.4.2-MariaDB
MAILER_DSN=smtp://your-account:your-password@your-smtp.com:465
MAILER_SENDER=loot@your-loot.io
# Redirect all emails to this address if you develop
MAILER_REDIRECT_TO=you@name.com
```

build the application

```
composer install --no-dev
yarn install
yarn encore prod
```

## Init of the database

Configure the database connexion by copying the database URL example from .env to .env.local and create it if does not
exist.

```
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force
php bin/console doctrine:migrations:version --add --all
```

## Possibles Errors

If you can this error :

```
The metadata storage is not initialized, please run the sync-metadata-stora  
ge command to fix this issue.   
```

Run :

```
php bin/console doctrine:migrations:sync-metadata-storage
```

## Configure a webserver

### Apache

Create a virtual host with the ```public``` folder as a document root. We provide a default [.htaccess](../support/apache/.htaccess) file you can copy
to the public folder.

```bash
cp support/apache/.htaccess public/.htaccess
```

## Test

Now you can sign in with the ```root``` user.

# Instructions for developpers

[[__TOC__]]

## Workflow to contribute

The worflow is based on the [Gitflow Workflow](https://www.atlassian.com/fr/git/tutorials/comparing-workflows/gitflow-workflow)
for developpers membres of the repository.

For external developpers, you have to follow the [Forking Workflow](https://www.atlassian.com/fr/git/tutorials/comparing-workflows/forking-workflow)

A governance draf paper published on 2022 explains the [rules to have a community driven features developpemnent](https://trello.com/c/WOC8UQZR/110-gouvernance-des-%C3%A9volutions-de-loot-pour-les-communaut%C3%A9s-utilisatrices)[french] :

- Howto use tags for issues
- Roles and limits of the contributors
- Howto deploy Loot in you community
- The contributory budget to maintain and distribute Loot

### Tags and Milestone to prepare a release

Developpers and mainteners indicate that they work on an issue by a tag `Planned`.

Loot does not have a calendar of releases. Each issues resolved is moved to the milestone `Next release` and merged to develop.
So users can always check the future features.

When it's important to delivery, we create a new release with a new version number and all issues 
actually in the `Next release` milestone will be in the delivery.

### Merge request for a new feature

Each merge request must be related to an issue created in the repository. To prepare a merge request you have to :

- Create or find issues related on
- Create a branch from develop branch and code your feature
- Create a merge request to ask to the mainteners that they should review it for integration

If you need to collaborate and have discussions about your code, you should create a draft merge request and
when you think that the maintener should approve the merge request, tag it as ready.

If you are not a developper authorized to the repository, you can fork the repository and suggest
a modification from a merge request.

After a merge request approved and done, set the milestone of issues to `Next release`.

> Important : use option --no-ff for merging a feature (https://nvie.com/posts/a-successful-git-branching-model/#incorporating-a-finished-feature-on-develop)
> We keep an history of the branch that we can remove after the merge

### Merge request for a hotfix

It's the same process that a feature but the based branch is the master branch.

### Merge request guidlines

- Squash commits if the MR have multiple small commits and edit the commit message to explain the feature
- Close the issues affected by the merge request evenif they are not merge in a release

### Delivery a new release (for mainteners)

When a next release is ready, rename the milestone `Next release` by its version number and create a new `Next release` milestone.
After approving by the community, we deploy the release with [the release feature of Gitlab](https://docs.gitlab.com/ee/user/project/releases/index.html#associate-milestones-with-a-release) 

Follow this steps

1. According to the [Gitflow Workflow](https://www.atlassian.com/fr/git/tutorials/comparing-workflows/gitflow-workflow), we create a delivery branch.

```bash
LOOT_VERSION=v1.1.4
git checkout develop
git checkout -b release/${LOOT_VERSION}
git push --set-upstream origin release/${LOOT_VERSION}
```

2. We tag the branch in order to create a Gitlab Release

```bash
git tag -a ${LOOT_VERSION} -m "${LOOT_VERSION}"
git push origin ${LOOT_VERSION}
```

3. From Gitlab, on the left sidebar, select Deployments > Releases.
4. Create a new release from the release tag
5. Fill information by selecting the tag, the milestone and with a title as the tag name and a description like this

> Release closing this issues : https://gitlab.com/loot-project/loot-app/-/issues?release_tag=`v1.1.0`&scope=all&state=closed

#### Deploy the release docker image

```bash
LOOT_VERSION=v1.1.4
git clone https://gitlab.com/loot-project/loot-app ./loot
cd loot
git checkout ${LOOT_VERSION}

```

Build and push a specific version

```bash
cd support/docker/production
docker login registry.gitlab.com
docker build -t registry.gitlab.com/loot-project/loot-app:${LOOT_VERSION} --file=Dockerfile ../../../
docker push registry.gitlab.com/loot-project/loot-app:${LOOT_VERSION}
```

At this point, we can deploy the proposition of this release to have feedback from the community

#### Improve and close the release delivery

Recommandation : create a merge request from the release to develop branch in order to review 
the differents fixes or have a discussion thread during the delivery.

If we have to fix something, 

1. we create a hotfix branch from the release branch (or we directly commit/push modifications).
   If we want, you can create a merge request to review the code.
2. we deploy again the release docker image for community validation

When the release is stable, we close it by

1. merging the release to develop if we have made changes in the release branch. If a merge request have been created, we close the merge.
2. merging the release to the master branch
3. Deploy the latest docker image, based on the release we close.

```bash
docker image tag registry.gitlab.com/loot-project/loot-app:${LOOT_VERSION} registry.gitlab.com/loot-project/loot-app:latest
docker push registry.gitlab.com/loot-project/loot-app:latest
```

Finaly, we close the milestone of the version.

#### Demo

The demo official demo is located in http://loot-demo.soletic.org/login 
with the demo docker image of Loot ([see container registry of the project](https://gitlab.com/loot-project/loot-app/container_registry/3668960)).

- Read [support/docker/demo/Readme.md](../support/docker/demo/Readme.md) to know how to build and deploy the demo.

## Install loot to contribute

The [support/docker/dev/Readme.md] explains how to setup Loot for developpers using Docker.

### Fixtures to load a dataset

The application uses the bundle https://github.com/hautelook/AliceBundle :

- Load fixtures to load dataset in your dev environmnet : `php bin/console hautelook:fixtures:load`
- Fixtures for phpunit

You got two users :

1. user1@localhost.com with one budget, one contribution and one retribution
2. user2@localhost.com with nothing
3. random data to test API with users api_X@localhost.com

They have `password` as password.

### Database Migration

Use [Doctrine Migrations](https://symfony.com/doc/current/bundles/DoctrineMigrationsBundle/index.html) to add migrations of the database.

```bash
php bin/console doctrine:migrations:diff
```

### Automating tests

We made a choice : create mainly [Application Tests](https://symfony.com/doc/current/testing.html#application-tests). 
Each functionnal test is created [tests/Functionnals](../tests/Functionnals) with a class name corresponds to the controller.

> We create unit test only for critical class.

To run tests configure your database URL in ```.env.test.local``` file and init it

```
php bin/console doctrine:database:drop --force --env=test
php bin/console doctrine:database:create --env=test
php bin/console doctrine:schema:update --force --env=test
```

PHPUnit tests use fixtures since 2021-03-30 so only few tests work with alice bundle

Now run tests

```
php vendor/bin/phpunit
```

May be : `php ./vendor/bin/phpunit` if the previous command does not work.

### Gitlab CI

To test pipeline, the [Dockerfile](../support/gitlab/Dockerfile) helps you. Build the image to test if the `before_script` of PhpUnit stage
will work.

```bash
cd support/gitlab
docker build -t local/loot-gitlabci -f ./Dockerfile ../../
```

Test the PhpUnit stage

```bash
# start database service
docker-compose up -d
# setup database
docker-compose run --rm loot php bin/console doctrine:database:create --env=test
docker-compose run --rm loot php bin/console doctrine:schema:update --force --env=test
# run test
docker-compose run --rm loot bash -c 'export SYMFONY_DEPRECATIONS_HELPER=weak ; vendor/bin/phpunit --coverage-text --colors=never'
```

## Roadmap and ideas

[ ] Activate php coding standards in pipeline and resolve errors

## Coding Standards

We use recommandations of Symfony : https://symfony.com/doc/current/contributing/code/standards.html

For developpers using PHPStorm, you can use the configuration file in `support/phpstorm` directory.

- [support/phpstorm/Symfony-Code-Style.xml](../support/phpstorm/Symfony-Code-Style.xml) : to generate the code
- [support/phpstorm/Inspections-Default-Profile.xml](../support/phpstorm/Inspections-Default-Profile.xml) : to display notifications about the code in the IDE 

PhpDOCS convevntions are used to help coding and generate documentation.

### CS Fixer

To fix coding standards, we use the PHP CS Fixer tool. You can install it with composer.

```bash
composer install --working-dir=./tools/php-cs-fixer
```

And run it

```bash
./tools/php-cs-fixer/vendor/bin/php-cs-fixer fix
```

## Markdown and translations

We have added the `markdown_to_html` twig filter to transform translations written in markdown to HTML and
continue using it in a context with no HTML.

## Send a mail to a contributor (user)

### Configure your environnement


Configure your `env` file (typically `.env.local` in root of this repos) :

```
MAILER_DSN=smtp://localhost
MAILER_SENDER=admin@loot.org
```

To set `MAILER_SENDER` this [page](https://symfony.com/doc/current/mailer.html#transport-setup) can you help.

### Sending Message in your code

You can add this in your Controler source code :

```php

use App\Service\ContributorMailer;

     public function une_fonction(ContributorMailer $mailer, UserRepository $userRepo, ...) {

        $user = $userRepo->find ...

        $mailer->sendMail($user, "Coucou !", "This is a message", "<p>This is a HTML message</p>");

// ...

    }


```

Or directly with a user id :

```php

use App\Service\ContributorMailer;

// ...

 public function une_fonction(ContributorMailer $mailer, $user_id, ...) {

        $mailer->sendMail($user_id, "Coucou !", "This is a message", "<p>This is a HTML message</p>");
 
// ...

    }

```
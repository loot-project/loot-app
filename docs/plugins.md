# Extend the application with Symfony Bundle as plugins

## Create a bundle to extend template

Follow instructions to create a bundle with [the symfony documentation](https://symfony.com/doc/current/bundles.html).
You must create the bundle in the ```plugins``` directory. The default namespace of the bundle will be ```Loot/Plugins```
as set up in the composer.json file.

It's possible to extend the configuration of the application with the mechanism offered by the interface ```PrependExtensionInterface```.
[Read this documentation to learn about it](https://symfony.com/doc/current/bundles/extension.html)

To get inspiration, you can read [the code of the DemoBundle](https://gitlab.com/loot-project/demo-bundle).
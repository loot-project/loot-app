# Basic instructions to upgrade Loot

Check the differents releases of the app here : https://gitlab.com/loot-project/loot-app/-/releases.

## With docker

- [Read the production support for docker](../support/docker/production/Readme.md)
- Read notes of the release you want to upgrade to in the file `UPGRADE-x.y.md` to understand the technical changes.

## Manual upgrade without docker

> Read the UPGRADE-x.y.md file of the release you want to upgrade to before starting it.

- [UPGRADE-1.1.md](../UPGRADE-1.1.md) : upgrade to 1.1.x (required before upgrading to 2.0.0)
- [UPGRADE-2.0.md](../UPGRADE-2.0.md) : upgrade from 1.1.4 to 2.x

### How to upgrade

- Download the release and unzip it in the directory of the app or git checkout the tag you want to upgrade to.
- Install the vendor libraries : `composer install --no-dev`
- Build the assets : `yarn install && yarn encore prod`

### About the database upgrade

Read the UPGRADE-x.y.md file to know the steps to upgrade the database.

## For Mainteners

### Upgrade libraries and symfony

We use Rector - https://getrector.com/ and CS Fixer - https://cs.symfony.com/ to upgrade and clean the code.

### Useful commands on symfony upgrade

```bash
# upgrade symfony : replace x.y.* by the new version and
composer up
# list libraties outdated
composer outdated
# upgrade only one library or multiple libraries
composer up|update 'symfony/*'
# play upgrade recipes
composer recipes:update
```

Recipes are a way to upgrade the code of the app to the new version of Symfony and we have to do it manually, one by one.

### Upgrade to Syfmony 6 - Ressources

- https://symfonycasts.com/screencast/symfony6-upgrade/

Read the upgrading guide of the current version of Symfony used by the app.

- For SF5 to 6 : https://symfony.com/doc/6.4/setup/upgrade_major.html
- For SF6 to 7 : https://symfony.com/doc/7.0/setup/upgrade_major.html